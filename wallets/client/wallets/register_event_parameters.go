// Code generated by go-swagger; DO NOT EDIT.

package wallets

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"

	"bitbucket.org/gank-global/clients/wallets/models"
)

// NewRegisterEventParams creates a new RegisterEventParams object
// with the default values initialized.
func NewRegisterEventParams() *RegisterEventParams {
	var ()
	return &RegisterEventParams{

		Timeout: cr.DefaultTimeout,
	}
}

// NewRegisterEventParamsWithTimeout creates a new RegisterEventParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewRegisterEventParamsWithTimeout(timeout time.Duration) *RegisterEventParams {
	var ()
	return &RegisterEventParams{

		Timeout: timeout,
	}
}

// NewRegisterEventParamsWithContext creates a new RegisterEventParams object
// with the default values initialized, and the ability to set a context for a request
func NewRegisterEventParamsWithContext(ctx context.Context) *RegisterEventParams {
	var ()
	return &RegisterEventParams{

		Context: ctx,
	}
}

// NewRegisterEventParamsWithHTTPClient creates a new RegisterEventParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewRegisterEventParamsWithHTTPClient(client *http.Client) *RegisterEventParams {
	var ()
	return &RegisterEventParams{
		HTTPClient: client,
	}
}

/*RegisterEventParams contains all the parameters to send to the API endpoint
for the register event operation typically these are written to a http.Request
*/
type RegisterEventParams struct {

	/*Content
	  payload

	*/
	Content *models.WalletRegisterEventInput

	Timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
	SMUserID   string
}

// WithTimeout adds the timeout to the register event params
func (o *RegisterEventParams) WithTimeout(timeout time.Duration) *RegisterEventParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the register event params
func (o *RegisterEventParams) SetTimeout(timeout time.Duration) {
	o.Timeout = timeout
}

// WithContext adds the context to the register event params
func (o *RegisterEventParams) WithContext(ctx context.Context) *RegisterEventParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the register event params
func (o *RegisterEventParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the register event params
func (o *RegisterEventParams) WithHTTPClient(client *http.Client) *RegisterEventParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the register event params
func (o *RegisterEventParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithContent adds the content to the register event params
func (o *RegisterEventParams) WithContent(content *models.WalletRegisterEventInput) *RegisterEventParams {
	o.SetContent(content)
	return o
}

// SetContent adds the content to the register event params
func (o *RegisterEventParams) SetContent(content *models.WalletRegisterEventInput) {
	o.Content = content
}

// WriteToRequest writes these params to a swagger request
func (o *RegisterEventParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {
	// header param ID
	if o.SMUserID != "" {
		if err := r.SetHeaderParam("X-Sm-User-ID", o.SMUserID); err != nil {
			return err
		}
	}
	if err := r.SetTimeout(o.Timeout); err != nil {
		return err
	}
	var res []error

	if o.Content != nil {
		if err := r.SetBodyParam(o.Content); err != nil {
			return err
		}
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
