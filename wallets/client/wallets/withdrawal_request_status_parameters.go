// Code generated by go-swagger; DO NOT EDIT.

package wallets

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"

	"bitbucket.org/gank-global/clients/wallets/models"
)

// NewWithdrawalRequestStatusParams creates a new WithdrawalRequestStatusParams object
// with the default values initialized.
func NewWithdrawalRequestStatusParams() *WithdrawalRequestStatusParams {
	var ()
	return &WithdrawalRequestStatusParams{

		Timeout: cr.DefaultTimeout,
	}
}

// NewWithdrawalRequestStatusParamsWithTimeout creates a new WithdrawalRequestStatusParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewWithdrawalRequestStatusParamsWithTimeout(timeout time.Duration) *WithdrawalRequestStatusParams {
	var ()
	return &WithdrawalRequestStatusParams{

		Timeout: timeout,
	}
}

// NewWithdrawalRequestStatusParamsWithContext creates a new WithdrawalRequestStatusParams object
// with the default values initialized, and the ability to set a context for a request
func NewWithdrawalRequestStatusParamsWithContext(ctx context.Context) *WithdrawalRequestStatusParams {
	var ()
	return &WithdrawalRequestStatusParams{

		Context: ctx,
	}
}

// NewWithdrawalRequestStatusParamsWithHTTPClient creates a new WithdrawalRequestStatusParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewWithdrawalRequestStatusParamsWithHTTPClient(client *http.Client) *WithdrawalRequestStatusParams {
	var ()
	return &WithdrawalRequestStatusParams{
		HTTPClient: client,
	}
}

/*WithdrawalRequestStatusParams contains all the parameters to send to the API endpoint
for the withdrawal request status operation typically these are written to a http.Request
*/
type WithdrawalRequestStatusParams struct {

	/*Content
	  payload

	*/
	Content *models.WalletUpdateWithdrawalInput
	/*ID
	  request id

	*/
	ID string

	Timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
	SMUserID   string
}

// WithTimeout adds the timeout to the withdrawal request status params
func (o *WithdrawalRequestStatusParams) WithTimeout(timeout time.Duration) *WithdrawalRequestStatusParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the withdrawal request status params
func (o *WithdrawalRequestStatusParams) SetTimeout(timeout time.Duration) {
	o.Timeout = timeout
}

// WithContext adds the context to the withdrawal request status params
func (o *WithdrawalRequestStatusParams) WithContext(ctx context.Context) *WithdrawalRequestStatusParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the withdrawal request status params
func (o *WithdrawalRequestStatusParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the withdrawal request status params
func (o *WithdrawalRequestStatusParams) WithHTTPClient(client *http.Client) *WithdrawalRequestStatusParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the withdrawal request status params
func (o *WithdrawalRequestStatusParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithContent adds the content to the withdrawal request status params
func (o *WithdrawalRequestStatusParams) WithContent(content *models.WalletUpdateWithdrawalInput) *WithdrawalRequestStatusParams {
	o.SetContent(content)
	return o
}

// SetContent adds the content to the withdrawal request status params
func (o *WithdrawalRequestStatusParams) SetContent(content *models.WalletUpdateWithdrawalInput) {
	o.Content = content
}

// WithID adds the id to the withdrawal request status params
func (o *WithdrawalRequestStatusParams) WithID(id string) *WithdrawalRequestStatusParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the withdrawal request status params
func (o *WithdrawalRequestStatusParams) SetID(id string) {
	o.ID = id
}

// WriteToRequest writes these params to a swagger request
func (o *WithdrawalRequestStatusParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {
	// header param ID
	if o.SMUserID != "" {
		if err := r.SetHeaderParam("X-Sm-User-ID", o.SMUserID); err != nil {
			return err
		}
	}
	if err := r.SetTimeout(o.Timeout); err != nil {
		return err
	}
	var res []error

	if o.Content != nil {
		if err := r.SetBodyParam(o.Content); err != nil {
			return err
		}
	}

	// path param id
	if err := r.SetPathParam("id", o.ID); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
