// Code generated by go-swagger; DO NOT EDIT.

package wallets

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"

	"bitbucket.org/gank-global/clients/wallets/models"
)

// NewCancelEventParams creates a new CancelEventParams object
// with the default values initialized.
func NewCancelEventParams() *CancelEventParams {
	var ()
	return &CancelEventParams{

		Timeout: cr.DefaultTimeout,
	}
}

// NewCancelEventParamsWithTimeout creates a new CancelEventParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewCancelEventParamsWithTimeout(timeout time.Duration) *CancelEventParams {
	var ()
	return &CancelEventParams{

		Timeout: timeout,
	}
}

// NewCancelEventParamsWithContext creates a new CancelEventParams object
// with the default values initialized, and the ability to set a context for a request
func NewCancelEventParamsWithContext(ctx context.Context) *CancelEventParams {
	var ()
	return &CancelEventParams{

		Context: ctx,
	}
}

// NewCancelEventParamsWithHTTPClient creates a new CancelEventParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewCancelEventParamsWithHTTPClient(client *http.Client) *CancelEventParams {
	var ()
	return &CancelEventParams{
		HTTPClient: client,
	}
}

/*CancelEventParams contains all the parameters to send to the API endpoint
for the cancel event operation typically these are written to a http.Request
*/
type CancelEventParams struct {

	/*Content
	  payload

	*/
	Content *models.WalletCancelEventInput

	Timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
	SMUserID   string
}

// WithTimeout adds the timeout to the cancel event params
func (o *CancelEventParams) WithTimeout(timeout time.Duration) *CancelEventParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the cancel event params
func (o *CancelEventParams) SetTimeout(timeout time.Duration) {
	o.Timeout = timeout
}

// WithContext adds the context to the cancel event params
func (o *CancelEventParams) WithContext(ctx context.Context) *CancelEventParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the cancel event params
func (o *CancelEventParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the cancel event params
func (o *CancelEventParams) WithHTTPClient(client *http.Client) *CancelEventParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the cancel event params
func (o *CancelEventParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithContent adds the content to the cancel event params
func (o *CancelEventParams) WithContent(content *models.WalletCancelEventInput) *CancelEventParams {
	o.SetContent(content)
	return o
}

// SetContent adds the content to the cancel event params
func (o *CancelEventParams) SetContent(content *models.WalletCancelEventInput) {
	o.Content = content
}

// WriteToRequest writes these params to a swagger request
func (o *CancelEventParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {
	// header param ID
	if o.SMUserID != "" {
		if err := r.SetHeaderParam("X-Sm-User-ID", o.SMUserID); err != nil {
			return err
		}
	}
	if err := r.SetTimeout(o.Timeout); err != nil {
		return err
	}
	var res []error

	if o.Content != nil {
		if err := r.SetBodyParam(o.Content); err != nil {
			return err
		}
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
