// Code generated by go-swagger; DO NOT EDIT.

package chats

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetHistoryParams creates a new GetHistoryParams object
// with the default values initialized.
func NewGetHistoryParams() *GetHistoryParams {
	var ()
	return &GetHistoryParams{

		Timeout: cr.DefaultTimeout,
	}
}

// NewGetHistoryParamsWithTimeout creates a new GetHistoryParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetHistoryParamsWithTimeout(timeout time.Duration) *GetHistoryParams {
	var ()
	return &GetHistoryParams{

		Timeout: timeout,
	}
}

// NewGetHistoryParamsWithContext creates a new GetHistoryParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetHistoryParamsWithContext(ctx context.Context) *GetHistoryParams {
	var ()
	return &GetHistoryParams{

		Context: ctx,
	}
}

// NewGetHistoryParamsWithHTTPClient creates a new GetHistoryParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetHistoryParamsWithHTTPClient(client *http.Client) *GetHistoryParams {
	var ()
	return &GetHistoryParams{
		HTTPClient: client,
	}
}

/*GetHistoryParams contains all the parameters to send to the API endpoint
for the get history operation typically these are written to a http.Request
*/
type GetHistoryParams struct {

	/*ChannelID*/
	ChannelID *string
	/*Direction*/
	Direction *string
	/*LastSeq*/
	LastSeq *int64
	/*Limit*/
	Limit *int64

	Timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
	SMUserID   string
}

// WithTimeout adds the timeout to the get history params
func (o *GetHistoryParams) WithTimeout(timeout time.Duration) *GetHistoryParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get history params
func (o *GetHistoryParams) SetTimeout(timeout time.Duration) {
	o.Timeout = timeout
}

// WithContext adds the context to the get history params
func (o *GetHistoryParams) WithContext(ctx context.Context) *GetHistoryParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get history params
func (o *GetHistoryParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get history params
func (o *GetHistoryParams) WithHTTPClient(client *http.Client) *GetHistoryParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get history params
func (o *GetHistoryParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithChannelID adds the channelID to the get history params
func (o *GetHistoryParams) WithChannelID(channelID *string) *GetHistoryParams {
	o.SetChannelID(channelID)
	return o
}

// SetChannelID adds the channelId to the get history params
func (o *GetHistoryParams) SetChannelID(channelID *string) {
	o.ChannelID = channelID
}

// WithDirection adds the direction to the get history params
func (o *GetHistoryParams) WithDirection(direction *string) *GetHistoryParams {
	o.SetDirection(direction)
	return o
}

// SetDirection adds the direction to the get history params
func (o *GetHistoryParams) SetDirection(direction *string) {
	o.Direction = direction
}

// WithLastSeq adds the lastSeq to the get history params
func (o *GetHistoryParams) WithLastSeq(lastSeq *int64) *GetHistoryParams {
	o.SetLastSeq(lastSeq)
	return o
}

// SetLastSeq adds the lastSeq to the get history params
func (o *GetHistoryParams) SetLastSeq(lastSeq *int64) {
	o.LastSeq = lastSeq
}

// WithLimit adds the limit to the get history params
func (o *GetHistoryParams) WithLimit(limit *int64) *GetHistoryParams {
	o.SetLimit(limit)
	return o
}

// SetLimit adds the limit to the get history params
func (o *GetHistoryParams) SetLimit(limit *int64) {
	o.Limit = limit
}

// WriteToRequest writes these params to a swagger request
func (o *GetHistoryParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {
	// header param ID
	if o.SMUserID != "" {
		if err := r.SetHeaderParam("X-Sm-User-ID", o.SMUserID); err != nil {
			return err
		}
	}
	if err := r.SetTimeout(o.Timeout); err != nil {
		return err
	}
	var res []error

	if o.ChannelID != nil {

		// query param ChannelID
		var qrChannelID string
		if o.ChannelID != nil {
			qrChannelID = *o.ChannelID
		}
		qChannelID := qrChannelID
		if qChannelID != "" {
			if err := r.SetQueryParam("ChannelID", qChannelID); err != nil {
				return err
			}
		}

	}

	if o.Direction != nil {

		// query param direction
		var qrDirection string
		if o.Direction != nil {
			qrDirection = *o.Direction
		}
		qDirection := qrDirection
		if qDirection != "" {
			if err := r.SetQueryParam("direction", qDirection); err != nil {
				return err
			}
		}

	}

	if o.LastSeq != nil {

		// query param lastSeq
		var qrLastSeq int64
		if o.LastSeq != nil {
			qrLastSeq = *o.LastSeq
		}
		qLastSeq := swag.FormatInt64(qrLastSeq)
		if qLastSeq != "" {
			if err := r.SetQueryParam("lastSeq", qLastSeq); err != nil {
				return err
			}
		}

	}

	if o.Limit != nil {

		// query param limit
		var qrLimit int64
		if o.Limit != nil {
			qrLimit = *o.Limit
		}
		qLimit := swag.FormatInt64(qrLimit)
		if qLimit != "" {
			if err := r.SetQueryParam("limit", qLimit); err != nil {
				return err
			}
		}

	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
