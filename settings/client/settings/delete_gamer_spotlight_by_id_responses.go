// Code generated by go-swagger; DO NOT EDIT.

package settings

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"

	models "bitbucket.org/gank-global/clients/settings/models"
)

// DeleteGamerSpotlightByIDReader is a Reader for the DeleteGamerSpotlightByID structure.
type DeleteGamerSpotlightByIDReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *DeleteGamerSpotlightByIDReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 200:
		result := NewDeleteGamerSpotlightByIDOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		result := NewResponseError()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result
	}
}

// NewDeleteGamerSpotlightByIDOK creates a DeleteGamerSpotlightByIDOK with default headers values
func NewDeleteGamerSpotlightByIDOK() *DeleteGamerSpotlightByIDOK {
	return &DeleteGamerSpotlightByIDOK{}
}

/*DeleteGamerSpotlightByIDOK handles this case with default header values.

OK
*/
type DeleteGamerSpotlightByIDOK struct {
}

func (o *DeleteGamerSpotlightByIDOK) Error() string {
	return fmt.Sprintf("[DELETE /settings/gamer-spotlights/{id}][%d] deleteGamerSpotlightByIdOK ", 200)
}

func (o *DeleteGamerSpotlightByIDOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}

/*DeleteGamerSpotlightByIDBadRequestBody delete gamer spotlight by ID bad request body
swagger:model DeleteGamerSpotlightByIDBadRequestBody
*/
type DeleteGamerSpotlightByIDBadRequestBody struct {

	// errors
	Errors *models.ErrorResponse `json:"errors,omitempty"`
}

// Validate validates this delete gamer spotlight by ID bad request body
func (o *DeleteGamerSpotlightByIDBadRequestBody) Validate(formats strfmt.Registry) error {
	var res []error

	if err := o.validateErrors(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *DeleteGamerSpotlightByIDBadRequestBody) validateErrors(formats strfmt.Registry) error {

	if swag.IsZero(o.Errors) { // not required
		return nil
	}

	if o.Errors != nil {
		if err := o.Errors.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("deleteGamerSpotlightByIdBadRequest" + "." + "errors")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (o *DeleteGamerSpotlightByIDBadRequestBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *DeleteGamerSpotlightByIDBadRequestBody) UnmarshalBinary(b []byte) error {
	var res DeleteGamerSpotlightByIDBadRequestBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}

/*DeleteGamerSpotlightByIDInternalServerErrorBody delete gamer spotlight by ID internal server error body
swagger:model DeleteGamerSpotlightByIDInternalServerErrorBody
*/
type DeleteGamerSpotlightByIDInternalServerErrorBody struct {

	// errors
	Errors *models.ErrorResponse `json:"errors,omitempty"`
}

// Validate validates this delete gamer spotlight by ID internal server error body
func (o *DeleteGamerSpotlightByIDInternalServerErrorBody) Validate(formats strfmt.Registry) error {
	var res []error

	if err := o.validateErrors(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *DeleteGamerSpotlightByIDInternalServerErrorBody) validateErrors(formats strfmt.Registry) error {

	if swag.IsZero(o.Errors) { // not required
		return nil
	}

	if o.Errors != nil {
		if err := o.Errors.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("deleteGamerSpotlightByIdInternalServerError" + "." + "errors")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (o *DeleteGamerSpotlightByIDInternalServerErrorBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *DeleteGamerSpotlightByIDInternalServerErrorBody) UnmarshalBinary(b []byte) error {
	var res DeleteGamerSpotlightByIDInternalServerErrorBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}

/*DeleteGamerSpotlightByIDUnprocessableEntityBody delete gamer spotlight by ID unprocessable entity body
swagger:model DeleteGamerSpotlightByIDUnprocessableEntityBody
*/
type DeleteGamerSpotlightByIDUnprocessableEntityBody struct {

	// errors
	Errors *models.ErrorResponse `json:"errors,omitempty"`
}

// Validate validates this delete gamer spotlight by ID unprocessable entity body
func (o *DeleteGamerSpotlightByIDUnprocessableEntityBody) Validate(formats strfmt.Registry) error {
	var res []error

	if err := o.validateErrors(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *DeleteGamerSpotlightByIDUnprocessableEntityBody) validateErrors(formats strfmt.Registry) error {

	if swag.IsZero(o.Errors) { // not required
		return nil
	}

	if o.Errors != nil {
		if err := o.Errors.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("deleteGamerSpotlightByIdUnprocessableEntity" + "." + "errors")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (o *DeleteGamerSpotlightByIDUnprocessableEntityBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *DeleteGamerSpotlightByIDUnprocessableEntityBody) UnmarshalBinary(b []byte) error {
	var res DeleteGamerSpotlightByIDUnprocessableEntityBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}
