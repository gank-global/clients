// Code generated by go-swagger; DO NOT EDIT.

package settings

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"

	models "bitbucket.org/gank-global/clients/settings/models"
)

// CreateVoucherReader is a Reader for the CreateVoucher structure.
type CreateVoucherReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *CreateVoucherReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 200:
		result := NewCreateVoucherOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		result := NewResponseError()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result
	}
}

// NewCreateVoucherOK creates a CreateVoucherOK with default headers values
func NewCreateVoucherOK() *CreateVoucherOK {
	return &CreateVoucherOK{}
}

/*CreateVoucherOK handles this case with default header values.

OK
*/
type CreateVoucherOK struct {
	Payload *CreateVoucherOKBody
}

func (o *CreateVoucherOK) Error() string {
	return fmt.Sprintf("[POST /settings/vouchers][%d] createVoucherOK  %+v", 200, o.Payload)
}

func (o *CreateVoucherOK) GetPayload() *CreateVoucherOKBody {
	return o.Payload
}

func (o *CreateVoucherOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(CreateVoucherOKBody)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

/*CreateVoucherBadRequestBody create voucher bad request body
swagger:model CreateVoucherBadRequestBody
*/
type CreateVoucherBadRequestBody struct {

	// errors
	Errors *models.ErrorResponse `json:"errors,omitempty"`
}

// Validate validates this create voucher bad request body
func (o *CreateVoucherBadRequestBody) Validate(formats strfmt.Registry) error {
	var res []error

	if err := o.validateErrors(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *CreateVoucherBadRequestBody) validateErrors(formats strfmt.Registry) error {

	if swag.IsZero(o.Errors) { // not required
		return nil
	}

	if o.Errors != nil {
		if err := o.Errors.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("createVoucherBadRequest" + "." + "errors")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (o *CreateVoucherBadRequestBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *CreateVoucherBadRequestBody) UnmarshalBinary(b []byte) error {
	var res CreateVoucherBadRequestBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}

/*CreateVoucherInternalServerErrorBody create voucher internal server error body
swagger:model CreateVoucherInternalServerErrorBody
*/
type CreateVoucherInternalServerErrorBody struct {

	// errors
	Errors *models.ErrorResponse `json:"errors,omitempty"`
}

// Validate validates this create voucher internal server error body
func (o *CreateVoucherInternalServerErrorBody) Validate(formats strfmt.Registry) error {
	var res []error

	if err := o.validateErrors(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *CreateVoucherInternalServerErrorBody) validateErrors(formats strfmt.Registry) error {

	if swag.IsZero(o.Errors) { // not required
		return nil
	}

	if o.Errors != nil {
		if err := o.Errors.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("createVoucherInternalServerError" + "." + "errors")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (o *CreateVoucherInternalServerErrorBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *CreateVoucherInternalServerErrorBody) UnmarshalBinary(b []byte) error {
	var res CreateVoucherInternalServerErrorBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}

/*CreateVoucherOKBody create voucher o k body
swagger:model CreateVoucherOKBody
*/
type CreateVoucherOKBody struct {

	// data
	Data *models.Voucher `json:"data,omitempty"`
}

// Validate validates this create voucher o k body
func (o *CreateVoucherOKBody) Validate(formats strfmt.Registry) error {
	var res []error

	if err := o.validateData(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *CreateVoucherOKBody) validateData(formats strfmt.Registry) error {

	if swag.IsZero(o.Data) { // not required
		return nil
	}

	if o.Data != nil {
		if err := o.Data.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("createVoucherOK" + "." + "data")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (o *CreateVoucherOKBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *CreateVoucherOKBody) UnmarshalBinary(b []byte) error {
	var res CreateVoucherOKBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}

/*CreateVoucherUnprocessableEntityBody create voucher unprocessable entity body
swagger:model CreateVoucherUnprocessableEntityBody
*/
type CreateVoucherUnprocessableEntityBody struct {

	// errors
	Errors *models.ErrorResponse `json:"errors,omitempty"`
}

// Validate validates this create voucher unprocessable entity body
func (o *CreateVoucherUnprocessableEntityBody) Validate(formats strfmt.Registry) error {
	var res []error

	if err := o.validateErrors(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *CreateVoucherUnprocessableEntityBody) validateErrors(formats strfmt.Registry) error {

	if swag.IsZero(o.Errors) { // not required
		return nil
	}

	if o.Errors != nil {
		if err := o.Errors.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("createVoucherUnprocessableEntity" + "." + "errors")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (o *CreateVoucherUnprocessableEntityBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *CreateVoucherUnprocessableEntityBody) UnmarshalBinary(b []byte) error {
	var res CreateVoucherUnprocessableEntityBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}
