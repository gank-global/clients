// Code generated by go-swagger; DO NOT EDIT.

package settings

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"

	models "bitbucket.org/gank-global/clients/settings/models"
)

// NewUpdateGamerSpotlightParams creates a new UpdateGamerSpotlightParams object
// with the default values initialized.
func NewUpdateGamerSpotlightParams() *UpdateGamerSpotlightParams {
	var ()
	return &UpdateGamerSpotlightParams{

		Timeout: cr.DefaultTimeout,
	}
}

// NewUpdateGamerSpotlightParamsWithTimeout creates a new UpdateGamerSpotlightParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewUpdateGamerSpotlightParamsWithTimeout(timeout time.Duration) *UpdateGamerSpotlightParams {
	var ()
	return &UpdateGamerSpotlightParams{

		Timeout: timeout,
	}
}

// NewUpdateGamerSpotlightParamsWithContext creates a new UpdateGamerSpotlightParams object
// with the default values initialized, and the ability to set a context for a request
func NewUpdateGamerSpotlightParamsWithContext(ctx context.Context) *UpdateGamerSpotlightParams {
	var ()
	return &UpdateGamerSpotlightParams{

		Context: ctx,
	}
}

// NewUpdateGamerSpotlightParamsWithHTTPClient creates a new UpdateGamerSpotlightParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewUpdateGamerSpotlightParamsWithHTTPClient(client *http.Client) *UpdateGamerSpotlightParams {
	var ()
	return &UpdateGamerSpotlightParams{
		HTTPClient: client,
	}
}

/*UpdateGamerSpotlightParams contains all the parameters to send to the API endpoint
for the update gamer spotlight operation typically these are written to a http.Request
*/
type UpdateGamerSpotlightParams struct {

	/*Content
	  GamerSpotlight payload

	*/
	Content *models.GamerSpotlightUpdateInput
	/*ID
	  GamerSpotlight ID

	*/
	ID string

	Timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
	SMUserID   string
}

// WithTimeout adds the timeout to the update gamer spotlight params
func (o *UpdateGamerSpotlightParams) WithTimeout(timeout time.Duration) *UpdateGamerSpotlightParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the update gamer spotlight params
func (o *UpdateGamerSpotlightParams) SetTimeout(timeout time.Duration) {
	o.Timeout = timeout
}

// WithContext adds the context to the update gamer spotlight params
func (o *UpdateGamerSpotlightParams) WithContext(ctx context.Context) *UpdateGamerSpotlightParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the update gamer spotlight params
func (o *UpdateGamerSpotlightParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the update gamer spotlight params
func (o *UpdateGamerSpotlightParams) WithHTTPClient(client *http.Client) *UpdateGamerSpotlightParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the update gamer spotlight params
func (o *UpdateGamerSpotlightParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithContent adds the content to the update gamer spotlight params
func (o *UpdateGamerSpotlightParams) WithContent(content *models.GamerSpotlightUpdateInput) *UpdateGamerSpotlightParams {
	o.SetContent(content)
	return o
}

// SetContent adds the content to the update gamer spotlight params
func (o *UpdateGamerSpotlightParams) SetContent(content *models.GamerSpotlightUpdateInput) {
	o.Content = content
}

// WithID adds the id to the update gamer spotlight params
func (o *UpdateGamerSpotlightParams) WithID(id string) *UpdateGamerSpotlightParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the update gamer spotlight params
func (o *UpdateGamerSpotlightParams) SetID(id string) {
	o.ID = id
}

// WriteToRequest writes these params to a swagger request
func (o *UpdateGamerSpotlightParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {
	// header param ID
	if o.SMUserID != "" {
		if err := r.SetHeaderParam("X-Sm-User-ID", o.SMUserID); err != nil {
			return err
		}
	}
	if err := r.SetTimeout(o.Timeout); err != nil {
		return err
	}
	var res []error

	if o.Content != nil {
		if err := r.SetBodyParam(o.Content); err != nil {
			return err
		}
	}

	// path param id
	if err := r.SetPathParam("id", o.ID); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
