// Code generated by go-swagger; DO NOT EDIT.

package settings

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewSpecializationGetListFilterParams creates a new SpecializationGetListFilterParams object
// with the default values initialized.
func NewSpecializationGetListFilterParams() *SpecializationGetListFilterParams {
	var ()
	return &SpecializationGetListFilterParams{

		Timeout: cr.DefaultTimeout,
	}
}

// NewSpecializationGetListFilterParamsWithTimeout creates a new SpecializationGetListFilterParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewSpecializationGetListFilterParamsWithTimeout(timeout time.Duration) *SpecializationGetListFilterParams {
	var ()
	return &SpecializationGetListFilterParams{

		Timeout: timeout,
	}
}

// NewSpecializationGetListFilterParamsWithContext creates a new SpecializationGetListFilterParams object
// with the default values initialized, and the ability to set a context for a request
func NewSpecializationGetListFilterParamsWithContext(ctx context.Context) *SpecializationGetListFilterParams {
	var ()
	return &SpecializationGetListFilterParams{

		Context: ctx,
	}
}

// NewSpecializationGetListFilterParamsWithHTTPClient creates a new SpecializationGetListFilterParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewSpecializationGetListFilterParamsWithHTTPClient(client *http.Client) *SpecializationGetListFilterParams {
	var ()
	return &SpecializationGetListFilterParams{
		HTTPClient: client,
	}
}

/*SpecializationGetListFilterParams contains all the parameters to send to the API endpoint
for the specialization get list filter operation typically these are written to a http.Request
*/
type SpecializationGetListFilterParams struct {

	/*GameID*/
	GameID *string
	/*Key*/
	Key *string
	/*OrderBy
	  Field which you want to order by

	*/
	OrderBy []string
	/*Page*/
	Page *int64
	/*PerPage*/
	PerPage *int64
	/*Value*/
	Value *string

	Timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
	SMUserID   string
}

// WithTimeout adds the timeout to the specialization get list filter params
func (o *SpecializationGetListFilterParams) WithTimeout(timeout time.Duration) *SpecializationGetListFilterParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the specialization get list filter params
func (o *SpecializationGetListFilterParams) SetTimeout(timeout time.Duration) {
	o.Timeout = timeout
}

// WithContext adds the context to the specialization get list filter params
func (o *SpecializationGetListFilterParams) WithContext(ctx context.Context) *SpecializationGetListFilterParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the specialization get list filter params
func (o *SpecializationGetListFilterParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the specialization get list filter params
func (o *SpecializationGetListFilterParams) WithHTTPClient(client *http.Client) *SpecializationGetListFilterParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the specialization get list filter params
func (o *SpecializationGetListFilterParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithGameID adds the gameID to the specialization get list filter params
func (o *SpecializationGetListFilterParams) WithGameID(gameID *string) *SpecializationGetListFilterParams {
	o.SetGameID(gameID)
	return o
}

// SetGameID adds the gameId to the specialization get list filter params
func (o *SpecializationGetListFilterParams) SetGameID(gameID *string) {
	o.GameID = gameID
}

// WithKey adds the key to the specialization get list filter params
func (o *SpecializationGetListFilterParams) WithKey(key *string) *SpecializationGetListFilterParams {
	o.SetKey(key)
	return o
}

// SetKey adds the key to the specialization get list filter params
func (o *SpecializationGetListFilterParams) SetKey(key *string) {
	o.Key = key
}

// WithOrderBy adds the orderBy to the specialization get list filter params
func (o *SpecializationGetListFilterParams) WithOrderBy(orderBy []string) *SpecializationGetListFilterParams {
	o.SetOrderBy(orderBy)
	return o
}

// SetOrderBy adds the orderBy to the specialization get list filter params
func (o *SpecializationGetListFilterParams) SetOrderBy(orderBy []string) {
	o.OrderBy = orderBy
}

// WithPage adds the page to the specialization get list filter params
func (o *SpecializationGetListFilterParams) WithPage(page *int64) *SpecializationGetListFilterParams {
	o.SetPage(page)
	return o
}

// SetPage adds the page to the specialization get list filter params
func (o *SpecializationGetListFilterParams) SetPage(page *int64) {
	o.Page = page
}

// WithPerPage adds the perPage to the specialization get list filter params
func (o *SpecializationGetListFilterParams) WithPerPage(perPage *int64) *SpecializationGetListFilterParams {
	o.SetPerPage(perPage)
	return o
}

// SetPerPage adds the perPage to the specialization get list filter params
func (o *SpecializationGetListFilterParams) SetPerPage(perPage *int64) {
	o.PerPage = perPage
}

// WithValue adds the value to the specialization get list filter params
func (o *SpecializationGetListFilterParams) WithValue(value *string) *SpecializationGetListFilterParams {
	o.SetValue(value)
	return o
}

// SetValue adds the value to the specialization get list filter params
func (o *SpecializationGetListFilterParams) SetValue(value *string) {
	o.Value = value
}

// WriteToRequest writes these params to a swagger request
func (o *SpecializationGetListFilterParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {
	// header param ID
	if o.SMUserID != "" {
		if err := r.SetHeaderParam("X-Sm-User-ID", o.SMUserID); err != nil {
			return err
		}
	}
	if err := r.SetTimeout(o.Timeout); err != nil {
		return err
	}
	var res []error

	if o.GameID != nil {

		// query param gameID
		var qrGameID string
		if o.GameID != nil {
			qrGameID = *o.GameID
		}
		qGameID := qrGameID
		if qGameID != "" {
			if err := r.SetQueryParam("gameID", qGameID); err != nil {
				return err
			}
		}

	}

	if o.Key != nil {

		// query param key
		var qrKey string
		if o.Key != nil {
			qrKey = *o.Key
		}
		qKey := qrKey
		if qKey != "" {
			if err := r.SetQueryParam("key", qKey); err != nil {
				return err
			}
		}

	}

	valuesOrderBy := o.OrderBy

	joinedOrderBy := swag.JoinByFormat(valuesOrderBy, "")
	// query array param order_by
	if err := r.SetQueryParam("order_by", joinedOrderBy...); err != nil {
		return err
	}

	if o.Page != nil {

		// query param page
		var qrPage int64
		if o.Page != nil {
			qrPage = *o.Page
		}
		qPage := swag.FormatInt64(qrPage)
		if qPage != "" {
			if err := r.SetQueryParam("page", qPage); err != nil {
				return err
			}
		}

	}

	if o.PerPage != nil {

		// query param per_page
		var qrPerPage int64
		if o.PerPage != nil {
			qrPerPage = *o.PerPage
		}
		qPerPage := swag.FormatInt64(qrPerPage)
		if qPerPage != "" {
			if err := r.SetQueryParam("per_page", qPerPage); err != nil {
				return err
			}
		}

	}

	if o.Value != nil {

		// query param value
		var qrValue string
		if o.Value != nil {
			qrValue = *o.Value
		}
		qValue := qrValue
		if qValue != "" {
			if err := r.SetQueryParam("value", qValue); err != nil {
				return err
			}
		}

	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
