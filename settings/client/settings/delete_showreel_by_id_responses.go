// Code generated by go-swagger; DO NOT EDIT.

package settings

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"

	models "bitbucket.org/gank-global/clients/settings/models"
)

// DeleteShowreelByIDReader is a Reader for the DeleteShowreelByID structure.
type DeleteShowreelByIDReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *DeleteShowreelByIDReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 200:
		result := NewDeleteShowreelByIDOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		result := NewResponseError()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result
	}
}

// NewDeleteShowreelByIDOK creates a DeleteShowreelByIDOK with default headers values
func NewDeleteShowreelByIDOK() *DeleteShowreelByIDOK {
	return &DeleteShowreelByIDOK{}
}

/*DeleteShowreelByIDOK handles this case with default header values.

OK
*/
type DeleteShowreelByIDOK struct {
}

func (o *DeleteShowreelByIDOK) Error() string {
	return fmt.Sprintf("[DELETE /settings/showreels/{id}][%d] deleteShowreelByIdOK ", 200)
}

func (o *DeleteShowreelByIDOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}

/*DeleteShowreelByIDBadRequestBody delete showreel by ID bad request body
swagger:model DeleteShowreelByIDBadRequestBody
*/
type DeleteShowreelByIDBadRequestBody struct {

	// errors
	Errors *models.ErrorResponse `json:"errors,omitempty"`
}

// Validate validates this delete showreel by ID bad request body
func (o *DeleteShowreelByIDBadRequestBody) Validate(formats strfmt.Registry) error {
	var res []error

	if err := o.validateErrors(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *DeleteShowreelByIDBadRequestBody) validateErrors(formats strfmt.Registry) error {

	if swag.IsZero(o.Errors) { // not required
		return nil
	}

	if o.Errors != nil {
		if err := o.Errors.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("deleteShowreelByIdBadRequest" + "." + "errors")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (o *DeleteShowreelByIDBadRequestBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *DeleteShowreelByIDBadRequestBody) UnmarshalBinary(b []byte) error {
	var res DeleteShowreelByIDBadRequestBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}

/*DeleteShowreelByIDInternalServerErrorBody delete showreel by ID internal server error body
swagger:model DeleteShowreelByIDInternalServerErrorBody
*/
type DeleteShowreelByIDInternalServerErrorBody struct {

	// errors
	Errors *models.ErrorResponse `json:"errors,omitempty"`
}

// Validate validates this delete showreel by ID internal server error body
func (o *DeleteShowreelByIDInternalServerErrorBody) Validate(formats strfmt.Registry) error {
	var res []error

	if err := o.validateErrors(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *DeleteShowreelByIDInternalServerErrorBody) validateErrors(formats strfmt.Registry) error {

	if swag.IsZero(o.Errors) { // not required
		return nil
	}

	if o.Errors != nil {
		if err := o.Errors.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("deleteShowreelByIdInternalServerError" + "." + "errors")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (o *DeleteShowreelByIDInternalServerErrorBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *DeleteShowreelByIDInternalServerErrorBody) UnmarshalBinary(b []byte) error {
	var res DeleteShowreelByIDInternalServerErrorBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}

/*DeleteShowreelByIDUnprocessableEntityBody delete showreel by ID unprocessable entity body
swagger:model DeleteShowreelByIDUnprocessableEntityBody
*/
type DeleteShowreelByIDUnprocessableEntityBody struct {

	// errors
	Errors *models.ErrorResponse `json:"errors,omitempty"`
}

// Validate validates this delete showreel by ID unprocessable entity body
func (o *DeleteShowreelByIDUnprocessableEntityBody) Validate(formats strfmt.Registry) error {
	var res []error

	if err := o.validateErrors(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *DeleteShowreelByIDUnprocessableEntityBody) validateErrors(formats strfmt.Registry) error {

	if swag.IsZero(o.Errors) { // not required
		return nil
	}

	if o.Errors != nil {
		if err := o.Errors.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("deleteShowreelByIdUnprocessableEntity" + "." + "errors")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (o *DeleteShowreelByIDUnprocessableEntityBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *DeleteShowreelByIDUnprocessableEntityBody) UnmarshalBinary(b []byte) error {
	var res DeleteShowreelByIDUnprocessableEntityBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}
