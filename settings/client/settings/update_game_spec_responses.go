// Code generated by go-swagger; DO NOT EDIT.

package settings

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"

	models "bitbucket.org/gank-global/clients/settings/models"
)

// UpdateGameSpecReader is a Reader for the UpdateGameSpec structure.
type UpdateGameSpecReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *UpdateGameSpecReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 200:
		result := NewUpdateGameSpecOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		result := NewResponseError()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result
	}
}

// NewUpdateGameSpecOK creates a UpdateGameSpecOK with default headers values
func NewUpdateGameSpecOK() *UpdateGameSpecOK {
	return &UpdateGameSpecOK{}
}

/*UpdateGameSpecOK handles this case with default header values.

OK
*/
type UpdateGameSpecOK struct {
	Payload *UpdateGameSpecOKBody
}

func (o *UpdateGameSpecOK) Error() string {
	return fmt.Sprintf("[PUT /settings/game-specs/{id}][%d] updateGameSpecOK  %+v", 200, o.Payload)
}

func (o *UpdateGameSpecOK) GetPayload() *UpdateGameSpecOKBody {
	return o.Payload
}

func (o *UpdateGameSpecOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(UpdateGameSpecOKBody)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

/*UpdateGameSpecBadRequestBody update game spec bad request body
swagger:model UpdateGameSpecBadRequestBody
*/
type UpdateGameSpecBadRequestBody struct {

	// errors
	Errors *models.ErrorResponse `json:"errors,omitempty"`
}

// Validate validates this update game spec bad request body
func (o *UpdateGameSpecBadRequestBody) Validate(formats strfmt.Registry) error {
	var res []error

	if err := o.validateErrors(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *UpdateGameSpecBadRequestBody) validateErrors(formats strfmt.Registry) error {

	if swag.IsZero(o.Errors) { // not required
		return nil
	}

	if o.Errors != nil {
		if err := o.Errors.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("updateGameSpecBadRequest" + "." + "errors")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (o *UpdateGameSpecBadRequestBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *UpdateGameSpecBadRequestBody) UnmarshalBinary(b []byte) error {
	var res UpdateGameSpecBadRequestBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}

/*UpdateGameSpecInternalServerErrorBody update game spec internal server error body
swagger:model UpdateGameSpecInternalServerErrorBody
*/
type UpdateGameSpecInternalServerErrorBody struct {

	// errors
	Errors *models.ErrorResponse `json:"errors,omitempty"`
}

// Validate validates this update game spec internal server error body
func (o *UpdateGameSpecInternalServerErrorBody) Validate(formats strfmt.Registry) error {
	var res []error

	if err := o.validateErrors(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *UpdateGameSpecInternalServerErrorBody) validateErrors(formats strfmt.Registry) error {

	if swag.IsZero(o.Errors) { // not required
		return nil
	}

	if o.Errors != nil {
		if err := o.Errors.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("updateGameSpecInternalServerError" + "." + "errors")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (o *UpdateGameSpecInternalServerErrorBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *UpdateGameSpecInternalServerErrorBody) UnmarshalBinary(b []byte) error {
	var res UpdateGameSpecInternalServerErrorBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}

/*UpdateGameSpecOKBody update game spec o k body
swagger:model UpdateGameSpecOKBody
*/
type UpdateGameSpecOKBody struct {

	// data
	Data *models.GameSpec `json:"data,omitempty"`
}

// Validate validates this update game spec o k body
func (o *UpdateGameSpecOKBody) Validate(formats strfmt.Registry) error {
	var res []error

	if err := o.validateData(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *UpdateGameSpecOKBody) validateData(formats strfmt.Registry) error {

	if swag.IsZero(o.Data) { // not required
		return nil
	}

	if o.Data != nil {
		if err := o.Data.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("updateGameSpecOK" + "." + "data")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (o *UpdateGameSpecOKBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *UpdateGameSpecOKBody) UnmarshalBinary(b []byte) error {
	var res UpdateGameSpecOKBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}

/*UpdateGameSpecUnprocessableEntityBody update game spec unprocessable entity body
swagger:model UpdateGameSpecUnprocessableEntityBody
*/
type UpdateGameSpecUnprocessableEntityBody struct {

	// errors
	Errors *models.ErrorResponse `json:"errors,omitempty"`
}

// Validate validates this update game spec unprocessable entity body
func (o *UpdateGameSpecUnprocessableEntityBody) Validate(formats strfmt.Registry) error {
	var res []error

	if err := o.validateErrors(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *UpdateGameSpecUnprocessableEntityBody) validateErrors(formats strfmt.Registry) error {

	if swag.IsZero(o.Errors) { // not required
		return nil
	}

	if o.Errors != nil {
		if err := o.Errors.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("updateGameSpecUnprocessableEntity" + "." + "errors")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (o *UpdateGameSpecUnprocessableEntityBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *UpdateGameSpecUnprocessableEntityBody) UnmarshalBinary(b []byte) error {
	var res UpdateGameSpecUnprocessableEntityBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}
