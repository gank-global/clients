// Code generated by go-swagger; DO NOT EDIT.

package settings

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewDeleteVoucherGeneratorParams creates a new DeleteVoucherGeneratorParams object
// with the default values initialized.
func NewDeleteVoucherGeneratorParams() *DeleteVoucherGeneratorParams {
	var ()
	return &DeleteVoucherGeneratorParams{

		Timeout: cr.DefaultTimeout,
	}
}

// NewDeleteVoucherGeneratorParamsWithTimeout creates a new DeleteVoucherGeneratorParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewDeleteVoucherGeneratorParamsWithTimeout(timeout time.Duration) *DeleteVoucherGeneratorParams {
	var ()
	return &DeleteVoucherGeneratorParams{

		Timeout: timeout,
	}
}

// NewDeleteVoucherGeneratorParamsWithContext creates a new DeleteVoucherGeneratorParams object
// with the default values initialized, and the ability to set a context for a request
func NewDeleteVoucherGeneratorParamsWithContext(ctx context.Context) *DeleteVoucherGeneratorParams {
	var ()
	return &DeleteVoucherGeneratorParams{

		Context: ctx,
	}
}

// NewDeleteVoucherGeneratorParamsWithHTTPClient creates a new DeleteVoucherGeneratorParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewDeleteVoucherGeneratorParamsWithHTTPClient(client *http.Client) *DeleteVoucherGeneratorParams {
	var ()
	return &DeleteVoucherGeneratorParams{
		HTTPClient: client,
	}
}

/*DeleteVoucherGeneratorParams contains all the parameters to send to the API endpoint
for the delete voucher generator operation typically these are written to a http.Request
*/
type DeleteVoucherGeneratorParams struct {

	/*ID
	  Voucher generator ID

	*/
	ID string

	Timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
	SMUserID   string
}

// WithTimeout adds the timeout to the delete voucher generator params
func (o *DeleteVoucherGeneratorParams) WithTimeout(timeout time.Duration) *DeleteVoucherGeneratorParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the delete voucher generator params
func (o *DeleteVoucherGeneratorParams) SetTimeout(timeout time.Duration) {
	o.Timeout = timeout
}

// WithContext adds the context to the delete voucher generator params
func (o *DeleteVoucherGeneratorParams) WithContext(ctx context.Context) *DeleteVoucherGeneratorParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the delete voucher generator params
func (o *DeleteVoucherGeneratorParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the delete voucher generator params
func (o *DeleteVoucherGeneratorParams) WithHTTPClient(client *http.Client) *DeleteVoucherGeneratorParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the delete voucher generator params
func (o *DeleteVoucherGeneratorParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithID adds the id to the delete voucher generator params
func (o *DeleteVoucherGeneratorParams) WithID(id string) *DeleteVoucherGeneratorParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the delete voucher generator params
func (o *DeleteVoucherGeneratorParams) SetID(id string) {
	o.ID = id
}

// WriteToRequest writes these params to a swagger request
func (o *DeleteVoucherGeneratorParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {
	// header param ID
	if o.SMUserID != "" {
		if err := r.SetHeaderParam("X-Sm-User-ID", o.SMUserID); err != nil {
			return err
		}
	}
	if err := r.SetTimeout(o.Timeout); err != nil {
		return err
	}
	var res []error

	// path param id
	if err := r.SetPathParam("id", o.ID); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
