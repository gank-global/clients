// Code generated by go-swagger; DO NOT EDIT.

package orders

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewOrderGetTotalFilterParams creates a new OrderGetTotalFilterParams object
// with the default values initialized.
func NewOrderGetTotalFilterParams() *OrderGetTotalFilterParams {
	var ()
	return &OrderGetTotalFilterParams{

		Timeout: cr.DefaultTimeout,
	}
}

// NewOrderGetTotalFilterParamsWithTimeout creates a new OrderGetTotalFilterParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewOrderGetTotalFilterParamsWithTimeout(timeout time.Duration) *OrderGetTotalFilterParams {
	var ()
	return &OrderGetTotalFilterParams{

		Timeout: timeout,
	}
}

// NewOrderGetTotalFilterParamsWithContext creates a new OrderGetTotalFilterParams object
// with the default values initialized, and the ability to set a context for a request
func NewOrderGetTotalFilterParamsWithContext(ctx context.Context) *OrderGetTotalFilterParams {
	var ()
	return &OrderGetTotalFilterParams{

		Context: ctx,
	}
}

// NewOrderGetTotalFilterParamsWithHTTPClient creates a new OrderGetTotalFilterParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewOrderGetTotalFilterParamsWithHTTPClient(client *http.Client) *OrderGetTotalFilterParams {
	var ()
	return &OrderGetTotalFilterParams{
		HTTPClient: client,
	}
}

/*OrderGetTotalFilterParams contains all the parameters to send to the API endpoint
for the order get total filter operation typically these are written to a http.Request
*/
type OrderGetTotalFilterParams struct {

	/*CoachID
	  Coach ID

	*/
	CoachID *string
	/*ServiceID
	  Service ID

	*/
	ServiceID *string
	/*Statuses
	  Order Statuses

	*/
	Statuses []string
	/*UserID
	  User ID

	*/
	UserID *string

	Timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
	SMUserID   string
}

// WithTimeout adds the timeout to the order get total filter params
func (o *OrderGetTotalFilterParams) WithTimeout(timeout time.Duration) *OrderGetTotalFilterParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the order get total filter params
func (o *OrderGetTotalFilterParams) SetTimeout(timeout time.Duration) {
	o.Timeout = timeout
}

// WithContext adds the context to the order get total filter params
func (o *OrderGetTotalFilterParams) WithContext(ctx context.Context) *OrderGetTotalFilterParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the order get total filter params
func (o *OrderGetTotalFilterParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the order get total filter params
func (o *OrderGetTotalFilterParams) WithHTTPClient(client *http.Client) *OrderGetTotalFilterParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the order get total filter params
func (o *OrderGetTotalFilterParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithCoachID adds the coachID to the order get total filter params
func (o *OrderGetTotalFilterParams) WithCoachID(coachID *string) *OrderGetTotalFilterParams {
	o.SetCoachID(coachID)
	return o
}

// SetCoachID adds the coachId to the order get total filter params
func (o *OrderGetTotalFilterParams) SetCoachID(coachID *string) {
	o.CoachID = coachID
}

// WithServiceID adds the serviceID to the order get total filter params
func (o *OrderGetTotalFilterParams) WithServiceID(serviceID *string) *OrderGetTotalFilterParams {
	o.SetServiceID(serviceID)
	return o
}

// SetServiceID adds the serviceId to the order get total filter params
func (o *OrderGetTotalFilterParams) SetServiceID(serviceID *string) {
	o.ServiceID = serviceID
}

// WithStatuses adds the statuses to the order get total filter params
func (o *OrderGetTotalFilterParams) WithStatuses(statuses []string) *OrderGetTotalFilterParams {
	o.SetStatuses(statuses)
	return o
}

// SetStatuses adds the statuses to the order get total filter params
func (o *OrderGetTotalFilterParams) SetStatuses(statuses []string) {
	o.Statuses = statuses
}

// WithUserID adds the userID to the order get total filter params
func (o *OrderGetTotalFilterParams) WithUserID(userID *string) *OrderGetTotalFilterParams {
	o.SetUserID(userID)
	return o
}

// SetUserID adds the userId to the order get total filter params
func (o *OrderGetTotalFilterParams) SetUserID(userID *string) {
	o.UserID = userID
}

// WriteToRequest writes these params to a swagger request
func (o *OrderGetTotalFilterParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {
	// header param ID
	if o.SMUserID != "" {
		if err := r.SetHeaderParam("X-Sm-User-ID", o.SMUserID); err != nil {
			return err
		}
	}
	if err := r.SetTimeout(o.Timeout); err != nil {
		return err
	}
	var res []error

	if o.CoachID != nil {

		// query param coachID
		var qrCoachID string
		if o.CoachID != nil {
			qrCoachID = *o.CoachID
		}
		qCoachID := qrCoachID
		if qCoachID != "" {
			if err := r.SetQueryParam("coachID", qCoachID); err != nil {
				return err
			}
		}

	}

	if o.ServiceID != nil {

		// query param serviceID
		var qrServiceID string
		if o.ServiceID != nil {
			qrServiceID = *o.ServiceID
		}
		qServiceID := qrServiceID
		if qServiceID != "" {
			if err := r.SetQueryParam("serviceID", qServiceID); err != nil {
				return err
			}
		}

	}

	valuesStatuses := o.Statuses

	joinedStatuses := swag.JoinByFormat(valuesStatuses, "")
	// query array param statuses
	if err := r.SetQueryParam("statuses", joinedStatuses...); err != nil {
		return err
	}

	if o.UserID != nil {

		// query param userID
		var qrUserID string
		if o.UserID != nil {
			qrUserID = *o.UserID
		}
		qUserID := qrUserID
		if qUserID != "" {
			if err := r.SetQueryParam("userID", qUserID); err != nil {
				return err
			}
		}

	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
