package client

import (
	"bitbucket.org/gank-global/clients/cloudflare/models"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strings"
)

type turnstileClient struct {
	apiKey     string
	baseUrl    string
	httpClient *http.Client
}

type TurnstileClient interface {
	Verify(ctx context.Context, token string) error
}

func NewTurnstileClient(key, baseURL string) TurnstileClient {
	return &turnstileClient{
		apiKey:     key,
		baseUrl:    baseURL,
		httpClient: &http.Client{},
	}
}

func (t *turnstileClient) Verify(ctx context.Context, token string) error {
	u := fmt.Sprintf("https://%s/turnstile/v0/siteverify", t.baseUrl)

	form := url.Values{}
	form.Add("secret", t.apiKey)
	form.Add("response", token)

	var output models.TurnstileResponse

	req, err := http.NewRequestWithContext(ctx, http.MethodPost, u, strings.NewReader(form.Encode()))
	if err != nil {
		return err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	res, err := t.httpClient.Do(req)
	if err != nil {
		return err
	}

	defer res.Body.Close()

	err = json.NewDecoder(res.Body).Decode(&output)
	if err != nil {
		return err
	}

	if !output.Success {
		msg := ""
		for _, code := range output.ErrorCodes {
			msg += fmt.Sprintf("%s\n", code)
		}

		return fmt.Errorf("error validate turnstile. error %s", msg)
	}

	return nil
}
