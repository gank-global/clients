package client

import (
	"context"
	"testing"
)

func Test_turnstileClient_Verify(t1 *testing.T) {
	type fields struct {
		apiKey  string
		baseUrl string
	}
	type args struct {
		ctx   context.Context
		token string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "turnstile validation",
			fields: fields{
				apiKey:  "0x4AAAAAAAIw0Op1VZZCLcBh5QgG0aw87bs",
				baseUrl: "challenges.cloudflare.com",
			},
			args: args{
				ctx:   context.Background(),
				token: "0.YS2_QuShCm1mefiMfx3p-dbS30U2w5GoU1Sa5a8Id2fTvdy6S2ZC3tl9xHvaikTbB2Mv4mjnNSr--DkC2gTzV-wxtyjg2R-NDw1UaS77xoDgJfzxlzsCWeeJRysetSavY69vg47T1Iy6XmJcQn7oAcdXKWMOTSlapug5bh6efxPJb89R0OmE92uvu0PSBo1YUjbRUBkH5MQ1ZIjYC1nTXavzUemicxeHx877NVSlKwBSFctWsf4408pb2woWAcMZo3zkTeXGcBy7eULs2VQiRwuZThDJ08e8pl4JbYHpQrhcYKI1ip6-Prg-ce1VV3gJJ3CH5zrNrdAWT7r7mHvB4Q4MfsNllUwvnMJ1zZ2d36ixNVTGMZu5nlbYyjblfasMyGt8iaOEVexl8LC7o8gUHeM57FGaQ4EIVRGIwLVfogmD6LLBuswWSnSuO4l1_G1F.gmhE5GsvP4R2A0vaZ2CJqA.5727854d2a91e1e8cc149713e89a506703862d63d0784225b5eea6f5c1eb8fa5",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := NewTurnstileClient(tt.fields.apiKey, tt.fields.baseUrl)

			if err := t.Verify(tt.args.ctx, tt.args.token); (err != nil) != tt.wantErr {
				t1.Errorf("Verify() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
