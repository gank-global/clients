// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"

	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// ModelPostMedia model post media
//
// swagger:model model.PostMedia
type ModelPostMedia struct {

	// blur Url
	BlurURL string `json:"blurUrl,omitempty"`

	// created at
	CreatedAt string `json:"createdAt,omitempty"`

	// id
	ID string `json:"id,omitempty"`

	// post ID
	PostID string `json:"postID,omitempty"`

	// preview Url
	PreviewURL string `json:"previewUrl,omitempty"`

	// thumb Url
	ThumbURL string `json:"thumbUrl,omitempty"`

	// type
	Type string `json:"type,omitempty"`

	// updated at
	UpdatedAt string `json:"updatedAt,omitempty"`

	// url
	URL string `json:"url,omitempty"`
}

// Validate validates this model post media
func (m *ModelPostMedia) Validate(formats strfmt.Registry) error {
	return nil
}

// ContextValidate validates this model post media based on context it is used
func (m *ModelPostMedia) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *ModelPostMedia) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *ModelPostMedia) UnmarshalBinary(b []byte) error {
	var res ModelPostMedia
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
