// Code generated by go-swagger; DO NOT EDIT.

package post_media

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"fmt"
	"io"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"

	"bitbucket.org/gank-global/clients/posts/models"
)

// GetV1PostsMediaByURLReader is a Reader for the GetV1PostsMediaByURL structure.
type GetV1PostsMediaByURLReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetV1PostsMediaByURLReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 200:
		result := NewGetV1PostsMediaByURLOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		result := NewResponseError()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result
	}
}

// NewGetV1PostsMediaByURLOK creates a GetV1PostsMediaByURLOK with default headers values
func NewGetV1PostsMediaByURLOK() *GetV1PostsMediaByURLOK {
	return &GetV1PostsMediaByURLOK{}
}

/*GetV1PostsMediaByURLOK handles this case with default header values.

OK
*/
type GetV1PostsMediaByURLOK struct {
	Payload *GetV1PostsMediaByURLOKBody
}

func (o *GetV1PostsMediaByURLOK) Error() string {
	return fmt.Sprintf("[GET /v1/posts/media/by-url][%d] getV1PostsMediaByUrlOK  %+v", 200, o.Payload)
}

func (o *GetV1PostsMediaByURLOK) GetPayload() *GetV1PostsMediaByURLOKBody {
	return o.Payload
}

func (o *GetV1PostsMediaByURLOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(GetV1PostsMediaByURLOKBody)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

/*GetV1PostsMediaByURLOKBody get v1 posts media by URL o k body
swagger:model GetV1PostsMediaByURLOKBody
*/

type GetV1PostsMediaByURLOKBody struct {
	models.ResponseAPIResponse

	// data
	Data *models.ModelPostMedia `json:"data,omitempty"`
}

// UnmarshalJSON unmarshals this object from a JSON structure
func (o *GetV1PostsMediaByURLOKBody) UnmarshalJSON(raw []byte) error {
	// GetV1PostsMediaByURLOKBodyAO0
	var getV1PostsMediaByURLOKBodyAO0 models.ResponseAPIResponse
	if err := swag.ReadJSON(raw, &getV1PostsMediaByURLOKBodyAO0); err != nil {
		return err
	}
	o.ResponseAPIResponse = getV1PostsMediaByURLOKBodyAO0

	// GetV1PostsMediaByURLOKBodyAO1
	var dataGetV1PostsMediaByURLOKBodyAO1 struct {
		Data *models.ModelPostMedia `json:"data,omitempty"`
	}
	if err := swag.ReadJSON(raw, &dataGetV1PostsMediaByURLOKBodyAO1); err != nil {
		return err
	}

	o.Data = dataGetV1PostsMediaByURLOKBodyAO1.Data

	return nil
}

// MarshalJSON marshals this object to a JSON structure
func (o GetV1PostsMediaByURLOKBody) MarshalJSON() ([]byte, error) {
	_parts := make([][]byte, 0, 2)

	getV1PostsMediaByURLOKBodyAO0, err := swag.WriteJSON(o.ResponseAPIResponse)
	if err != nil {
		return nil, err
	}
	_parts = append(_parts, getV1PostsMediaByURLOKBodyAO0)
	var dataGetV1PostsMediaByURLOKBodyAO1 struct {
		Data *models.ModelPostMedia `json:"data,omitempty"`
	}

	dataGetV1PostsMediaByURLOKBodyAO1.Data = o.Data

	jsonDataGetV1PostsMediaByURLOKBodyAO1, errGetV1PostsMediaByURLOKBodyAO1 := swag.WriteJSON(dataGetV1PostsMediaByURLOKBodyAO1)
	if errGetV1PostsMediaByURLOKBodyAO1 != nil {
		return nil, errGetV1PostsMediaByURLOKBodyAO1
	}
	_parts = append(_parts, jsonDataGetV1PostsMediaByURLOKBodyAO1)
	return swag.ConcatJSON(_parts...), nil
}

// Validate validates this get v1 posts media by URL o k body
func (o *GetV1PostsMediaByURLOKBody) Validate(formats strfmt.Registry) error {
	var res []error

	// validation for a type composition with models.ResponseAPIResponse
	if err := o.ResponseAPIResponse.Validate(formats); err != nil {
		res = append(res, err)
	}

	if err := o.validateData(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *GetV1PostsMediaByURLOKBody) validateData(formats strfmt.Registry) error {

	if swag.IsZero(o.Data) { // not required
		return nil
	}

	if o.Data != nil {
		if err := o.Data.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("getV1PostsMediaByUrlOK" + "." + "data")
			}
			return err
		}
	}

	return nil
}

// ContextValidate validate this get v1 posts media by URL o k body based on the context it is used
func (o *GetV1PostsMediaByURLOKBody) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	var res []error

	// validation for a type composition with models.ResponseAPIResponse
	if err := o.ResponseAPIResponse.ContextValidate(ctx, formats); err != nil {
		res = append(res, err)
	}

	if err := o.contextValidateData(ctx, formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *GetV1PostsMediaByURLOKBody) contextValidateData(ctx context.Context, formats strfmt.Registry) error {

	if o.Data != nil {
		if err := o.Data.ContextValidate(ctx, formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("getV1PostsMediaByUrlOK" + "." + "data")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (o *GetV1PostsMediaByURLOKBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *GetV1PostsMediaByURLOKBody) UnmarshalBinary(b []byte) error {
	var res GetV1PostsMediaByURLOKBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}
