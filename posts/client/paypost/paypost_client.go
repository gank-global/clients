// Code generated by go-swagger; DO NOT EDIT.

package paypost

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// New creates a new paypost API client.
func New(transport runtime.ClientTransport, formats strfmt.Registry) *Client {
	return &Client{transport: transport, formats: formats}
}

/*
Client for paypost API
*/
type Client struct {
	transport runtime.ClientTransport
	formats   strfmt.Registry
}

/*
PaypostCreateHandler creates paypost

Create paypost
*/
func (a *Client) PaypostCreateHandler(params *PaypostCreateHandlerParams) (*PaypostCreateHandlerOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewPaypostCreateHandlerParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "PaypostCreateHandler",
		Method:             "POST",
		PathPattern:        "/v1/posts/paypost",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"http"},
		Params:             params,
		Reader:             &PaypostCreateHandlerReader{formats: a.formats},
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	success, ok := result.(*PaypostCreateHandlerOK)
	if ok {
		return success, nil
	}
	// unexpected success response
	// safeguard: normally, absent a default response, unknown success responses return an error above: so this is a codegen issue
	msg := fmt.Sprintf("unexpected success response for PaypostCreateHandler: API contract not enforced by server. Client expected to get an error, but got: %T", result)
	panic(msg)
}

/*
PaypostListHandler lists pay post

List pay post
*/
func (a *Client) PaypostListHandler(params *PaypostListHandlerParams) (*PaypostListHandlerOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewPaypostListHandlerParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "PaypostListHandler",
		Method:             "GET",
		PathPattern:        "/v1/posts/paypost",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"http"},
		Params:             params,
		Reader:             &PaypostListHandlerReader{formats: a.formats},
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	success, ok := result.(*PaypostListHandlerOK)
	if ok {
		return success, nil
	}
	// unexpected success response
	// safeguard: normally, absent a default response, unknown success responses return an error above: so this is a codegen issue
	msg := fmt.Sprintf("unexpected success response for PaypostListHandler: API contract not enforced by server. Client expected to get an error, but got: %T", result)
	panic(msg)
}

type ResponseError struct {
	Payload *ResponseErrorBody
	Code    int64
	Message string
}

func NewResponseError() *ResponseError {
	return &ResponseError{}
}

func (o *ResponseError) Error() string {
	var code int64
	var msg interface{}
	if o.Payload != nil && o.Payload.Errors != nil {
		code = o.Payload.Errors.Code
		msg = o.Payload.Errors
	} else {
		code = o.Code
		msg = o.Message
	}
	return fmt.Sprintf("ResponseStatus[%d] ResponseError  %+v", code, msg)
}

func (o *ResponseError) GetPayload() *ResponseErrorBody {
	return o.Payload
}

func (o *ResponseError) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(ResponseErrorBody)
	o.Code = int64(response.Code())
	o.Message = response.Message()

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

/*ResponseErrorBody
  swagger:model ResponseErrorBody
*/
type ResponseErrorBody struct {

	// errors
	Errors *ErrorResponse `json:"errors,omitempty"`
}

// MarshalBinary interface implementation
func (o *ResponseErrorBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *ResponseErrorBody) UnmarshalBinary(b []byte) error {
	var res ResponseErrorBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}

type ErrorResponse struct {

	// code
	Code int64 `json:"code,omitempty"`

	// errors
	Errors map[string][]string `json:"errors,omitempty"`

	// message
	Message string `json:"message,omitempty"`
}

// Validate validates this error response
func (m *ErrorResponse) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *ErrorResponse) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *ErrorResponse) UnmarshalBinary(b []byte) error {
	var res ErrorResponse
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
