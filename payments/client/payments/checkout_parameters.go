// Code generated by go-swagger; DO NOT EDIT.

package payments

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"

	"bitbucket.org/gank-global/clients/payments/models"
)

// NewCheckoutParams creates a new CheckoutParams object
// with the default values initialized.
func NewCheckoutParams() *CheckoutParams {
	var ()
	return &CheckoutParams{

		Timeout: cr.DefaultTimeout,
	}
}

// NewCheckoutParamsWithTimeout creates a new CheckoutParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewCheckoutParamsWithTimeout(timeout time.Duration) *CheckoutParams {
	var ()
	return &CheckoutParams{

		Timeout: timeout,
	}
}

// NewCheckoutParamsWithContext creates a new CheckoutParams object
// with the default values initialized, and the ability to set a context for a request
func NewCheckoutParamsWithContext(ctx context.Context) *CheckoutParams {
	var ()
	return &CheckoutParams{

		Context: ctx,
	}
}

// NewCheckoutParamsWithHTTPClient creates a new CheckoutParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewCheckoutParamsWithHTTPClient(client *http.Client) *CheckoutParams {
	var ()
	return &CheckoutParams{
		HTTPClient: client,
	}
}

/*CheckoutParams contains all the parameters to send to the API endpoint
for the checkout operation typically these are written to a http.Request
*/
type CheckoutParams struct {

	/*Content
	  payload

	*/
	Content *models.DtoPaymentCheckoutInput

	Timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
	SMUserID   string
}

// WithTimeout adds the timeout to the checkout params
func (o *CheckoutParams) WithTimeout(timeout time.Duration) *CheckoutParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the checkout params
func (o *CheckoutParams) SetTimeout(timeout time.Duration) {
	o.Timeout = timeout
}

// WithContext adds the context to the checkout params
func (o *CheckoutParams) WithContext(ctx context.Context) *CheckoutParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the checkout params
func (o *CheckoutParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the checkout params
func (o *CheckoutParams) WithHTTPClient(client *http.Client) *CheckoutParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the checkout params
func (o *CheckoutParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithContent adds the content to the checkout params
func (o *CheckoutParams) WithContent(content *models.DtoPaymentCheckoutInput) *CheckoutParams {
	o.SetContent(content)
	return o
}

// SetContent adds the content to the checkout params
func (o *CheckoutParams) SetContent(content *models.DtoPaymentCheckoutInput) {
	o.Content = content
}

// WriteToRequest writes these params to a swagger request
func (o *CheckoutParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {
	// header param ID
	if o.SMUserID != "" {
		if err := r.SetHeaderParam("X-Sm-User-ID", o.SMUserID); err != nil {
			return err
		}
	}
	if err := r.SetTimeout(o.Timeout); err != nil {
		return err
	}
	var res []error

	if o.Content != nil {
		if err := r.SetBodyParam(o.Content); err != nil {
			return err
		}
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
