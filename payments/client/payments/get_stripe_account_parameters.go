// Code generated by go-swagger; DO NOT EDIT.

package payments

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetStripeAccountParams creates a new GetStripeAccountParams object
// with the default values initialized.
func NewGetStripeAccountParams() *GetStripeAccountParams {

	return &GetStripeAccountParams{

		Timeout: cr.DefaultTimeout,
	}
}

// NewGetStripeAccountParamsWithTimeout creates a new GetStripeAccountParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetStripeAccountParamsWithTimeout(timeout time.Duration) *GetStripeAccountParams {

	return &GetStripeAccountParams{

		Timeout: timeout,
	}
}

// NewGetStripeAccountParamsWithContext creates a new GetStripeAccountParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetStripeAccountParamsWithContext(ctx context.Context) *GetStripeAccountParams {

	return &GetStripeAccountParams{

		Context: ctx,
	}
}

// NewGetStripeAccountParamsWithHTTPClient creates a new GetStripeAccountParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetStripeAccountParamsWithHTTPClient(client *http.Client) *GetStripeAccountParams {

	return &GetStripeAccountParams{
		HTTPClient: client,
	}
}

/*GetStripeAccountParams contains all the parameters to send to the API endpoint
for the get stripe account operation typically these are written to a http.Request
*/
type GetStripeAccountParams struct {
	Timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
	SMUserID   string
}

// WithTimeout adds the timeout to the get stripe account params
func (o *GetStripeAccountParams) WithTimeout(timeout time.Duration) *GetStripeAccountParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get stripe account params
func (o *GetStripeAccountParams) SetTimeout(timeout time.Duration) {
	o.Timeout = timeout
}

// WithContext adds the context to the get stripe account params
func (o *GetStripeAccountParams) WithContext(ctx context.Context) *GetStripeAccountParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get stripe account params
func (o *GetStripeAccountParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get stripe account params
func (o *GetStripeAccountParams) WithHTTPClient(client *http.Client) *GetStripeAccountParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get stripe account params
func (o *GetStripeAccountParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WriteToRequest writes these params to a swagger request
func (o *GetStripeAccountParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {
	// header param ID
	if o.SMUserID != "" {
		if err := r.SetHeaderParam("X-Sm-User-ID", o.SMUserID); err != nil {
			return err
		}
	}
	if err := r.SetTimeout(o.Timeout); err != nil {
		return err
	}
	var res []error

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
