// Code generated by go-swagger; DO NOT EDIT.

package payments

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"

	"bitbucket.org/gank-global/clients/payments/models"
)

// NewCreatePaymentMethodParams creates a new CreatePaymentMethodParams object
// with the default values initialized.
func NewCreatePaymentMethodParams() *CreatePaymentMethodParams {
	var ()
	return &CreatePaymentMethodParams{

		Timeout: cr.DefaultTimeout,
	}
}

// NewCreatePaymentMethodParamsWithTimeout creates a new CreatePaymentMethodParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewCreatePaymentMethodParamsWithTimeout(timeout time.Duration) *CreatePaymentMethodParams {
	var ()
	return &CreatePaymentMethodParams{

		Timeout: timeout,
	}
}

// NewCreatePaymentMethodParamsWithContext creates a new CreatePaymentMethodParams object
// with the default values initialized, and the ability to set a context for a request
func NewCreatePaymentMethodParamsWithContext(ctx context.Context) *CreatePaymentMethodParams {
	var ()
	return &CreatePaymentMethodParams{

		Context: ctx,
	}
}

// NewCreatePaymentMethodParamsWithHTTPClient creates a new CreatePaymentMethodParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewCreatePaymentMethodParamsWithHTTPClient(client *http.Client) *CreatePaymentMethodParams {
	var ()
	return &CreatePaymentMethodParams{
		HTTPClient: client,
	}
}

/*CreatePaymentMethodParams contains all the parameters to send to the API endpoint
for the create payment method operation typically these are written to a http.Request
*/
type CreatePaymentMethodParams struct {

	/*Content
	  payment method create payload

	*/
	Content *models.PaymentAccountAddPaymentMethodInput

	Timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
	SMUserID   string
}

// WithTimeout adds the timeout to the create payment method params
func (o *CreatePaymentMethodParams) WithTimeout(timeout time.Duration) *CreatePaymentMethodParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the create payment method params
func (o *CreatePaymentMethodParams) SetTimeout(timeout time.Duration) {
	o.Timeout = timeout
}

// WithContext adds the context to the create payment method params
func (o *CreatePaymentMethodParams) WithContext(ctx context.Context) *CreatePaymentMethodParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the create payment method params
func (o *CreatePaymentMethodParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the create payment method params
func (o *CreatePaymentMethodParams) WithHTTPClient(client *http.Client) *CreatePaymentMethodParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the create payment method params
func (o *CreatePaymentMethodParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithContent adds the content to the create payment method params
func (o *CreatePaymentMethodParams) WithContent(content *models.PaymentAccountAddPaymentMethodInput) *CreatePaymentMethodParams {
	o.SetContent(content)
	return o
}

// SetContent adds the content to the create payment method params
func (o *CreatePaymentMethodParams) SetContent(content *models.PaymentAccountAddPaymentMethodInput) {
	o.Content = content
}

// WriteToRequest writes these params to a swagger request
func (o *CreatePaymentMethodParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {
	// header param ID
	if o.SMUserID != "" {
		if err := r.SetHeaderParam("X-Sm-User-ID", o.SMUserID); err != nil {
			return err
		}
	}
	if err := r.SetTimeout(o.Timeout); err != nil {
		return err
	}
	var res []error

	if o.Content != nil {
		if err := r.SetBodyParam(o.Content); err != nil {
			return err
		}
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
