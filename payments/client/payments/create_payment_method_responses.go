// Code generated by go-swagger; DO NOT EDIT.

package payments

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"
)

// CreatePaymentMethodReader is a Reader for the CreatePaymentMethod structure.
type CreatePaymentMethodReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *CreatePaymentMethodReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 200:
		result := NewCreatePaymentMethodOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		result := NewResponseError()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result
	}
}

// NewCreatePaymentMethodOK creates a CreatePaymentMethodOK with default headers values
func NewCreatePaymentMethodOK() *CreatePaymentMethodOK {
	return &CreatePaymentMethodOK{}
}

/*CreatePaymentMethodOK handles this case with default header values.

OK
*/
type CreatePaymentMethodOK struct {
}

func (o *CreatePaymentMethodOK) Error() string {
	return fmt.Sprintf("[POST /payments/stripe/accounts/me/payment-methods][%d] createPaymentMethodOK ", 200)
}

func (o *CreatePaymentMethodOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}
