// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// StripeAccountAddress stripe account address
//
// swagger:model stripe.AccountAddress
type StripeAccountAddress struct {

	// city
	City string `json:"city,omitempty"`

	// country
	Country string `json:"country,omitempty"`

	// line1
	Line1 string `json:"line1,omitempty"`

	// line2
	Line2 string `json:"line2,omitempty"`

	// postal code
	PostalCode string `json:"postal_code,omitempty"`

	// state
	State string `json:"state,omitempty"`

	// Town/cho-me. Note that this is only used for Kana/Kanji representations
	// of an address.
	Town string `json:"town,omitempty"`
}

// Validate validates this stripe account address
func (m *StripeAccountAddress) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *StripeAccountAddress) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *StripeAccountAddress) UnmarshalBinary(b []byte) error {
	var res StripeAccountAddress
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
