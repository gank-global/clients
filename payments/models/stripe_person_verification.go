// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"github.com/go-openapi/errors"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// StripePersonVerification stripe person verification
//
// swagger:model stripe.PersonVerification
type StripePersonVerification struct {

	// additional document
	AdditionalDocument *StripePersonVerificationDocument `json:"additional_document,omitempty"`

	// details
	Details string `json:"details,omitempty"`

	// details code
	DetailsCode string `json:"details_code,omitempty"`

	// document
	Document *StripePersonVerificationDocument `json:"document,omitempty"`

	// status
	Status string `json:"status,omitempty"`
}

// Validate validates this stripe person verification
func (m *StripePersonVerification) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateAdditionalDocument(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateDocument(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *StripePersonVerification) validateAdditionalDocument(formats strfmt.Registry) error {

	if swag.IsZero(m.AdditionalDocument) { // not required
		return nil
	}

	if m.AdditionalDocument != nil {
		if err := m.AdditionalDocument.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("additional_document")
			}
			return err
		}
	}

	return nil
}

func (m *StripePersonVerification) validateDocument(formats strfmt.Registry) error {

	if swag.IsZero(m.Document) { // not required
		return nil
	}

	if m.Document != nil {
		if err := m.Document.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("document")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (m *StripePersonVerification) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *StripePersonVerification) UnmarshalBinary(b []byte) error {
	var res StripePersonVerification
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
