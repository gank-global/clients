// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"strconv"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// StripeDispute stripe dispute
//
// swagger:model stripe.Dispute
type StripeDispute struct {

	// amount
	Amount int64 `json:"amount,omitempty"`

	// balance transactions
	BalanceTransactions []*StripeBalanceTransaction `json:"balance_transactions"`

	// charge
	Charge *StripeCharge `json:"charge,omitempty"`

	// created
	Created int64 `json:"created,omitempty"`

	// currency
	Currency string `json:"currency,omitempty"`

	// evidence
	Evidence *StripeDisputeEvidence `json:"evidence,omitempty"`

	// evidence details
	EvidenceDetails *StripeEvidenceDetails `json:"evidence_details,omitempty"`

	// id
	ID string `json:"id,omitempty"`

	// is charge refundable
	IsChargeRefundable bool `json:"is_charge_refundable,omitempty"`

	// livemode
	Livemode bool `json:"livemode,omitempty"`

	// metadata
	Metadata map[string]string `json:"metadata,omitempty"`

	// payment intent
	PaymentIntent *StripePaymentIntent `json:"payment_intent,omitempty"`

	// reason
	Reason string `json:"reason,omitempty"`

	// status
	Status string `json:"status,omitempty"`
}

// Validate validates this stripe dispute
func (m *StripeDispute) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateBalanceTransactions(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateCharge(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateEvidence(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateEvidenceDetails(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validatePaymentIntent(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *StripeDispute) validateBalanceTransactions(formats strfmt.Registry) error {

	if swag.IsZero(m.BalanceTransactions) { // not required
		return nil
	}

	for i := 0; i < len(m.BalanceTransactions); i++ {
		if swag.IsZero(m.BalanceTransactions[i]) { // not required
			continue
		}

		if m.BalanceTransactions[i] != nil {
			if err := m.BalanceTransactions[i].Validate(formats); err != nil {
				if ve, ok := err.(*errors.Validation); ok {
					return ve.ValidateName("balance_transactions" + "." + strconv.Itoa(i))
				}
				return err
			}
		}

	}

	return nil
}

func (m *StripeDispute) validateCharge(formats strfmt.Registry) error {

	if swag.IsZero(m.Charge) { // not required
		return nil
	}

	if m.Charge != nil {
		if err := m.Charge.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("charge")
			}
			return err
		}
	}

	return nil
}

func (m *StripeDispute) validateEvidence(formats strfmt.Registry) error {

	if swag.IsZero(m.Evidence) { // not required
		return nil
	}

	if m.Evidence != nil {
		if err := m.Evidence.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("evidence")
			}
			return err
		}
	}

	return nil
}

func (m *StripeDispute) validateEvidenceDetails(formats strfmt.Registry) error {

	if swag.IsZero(m.EvidenceDetails) { // not required
		return nil
	}

	if m.EvidenceDetails != nil {
		if err := m.EvidenceDetails.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("evidence_details")
			}
			return err
		}
	}

	return nil
}

func (m *StripeDispute) validatePaymentIntent(formats strfmt.Registry) error {

	if swag.IsZero(m.PaymentIntent) { // not required
		return nil
	}

	if m.PaymentIntent != nil {
		if err := m.PaymentIntent.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("payment_intent")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (m *StripeDispute) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *StripeDispute) UnmarshalBinary(b []byte) error {
	var res StripeDispute
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
