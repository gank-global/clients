// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"github.com/go-openapi/errors"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// StripeChargePaymentMethodDetailsCard stripe charge payment method details card
//
// swagger:model stripe.ChargePaymentMethodDetailsCard
type StripeChargePaymentMethodDetailsCard struct {

	// brand
	Brand string `json:"brand,omitempty"`

	// checks
	Checks *StripeChargePaymentMethodDetailsCardChecks `json:"checks,omitempty"`

	// country
	Country string `json:"country,omitempty"`

	// Please note that the fields below are for internal use only and are not returned
	// as part of standard API requests.
	Description string `json:"description,omitempty"`

	// exp month
	ExpMonth int64 `json:"exp_month,omitempty"`

	// exp year
	ExpYear int64 `json:"exp_year,omitempty"`

	// fingerprint
	Fingerprint string `json:"fingerprint,omitempty"`

	// funding
	Funding string `json:"funding,omitempty"`

	// iin
	Iin string `json:"iin,omitempty"`

	// installments
	Installments *StripeChargePaymentMethodDetailsCardInstallments `json:"installments,omitempty"`

	// issuer
	Issuer string `json:"issuer,omitempty"`

	// last4
	Last4 string `json:"last4,omitempty"`

	// moto
	Moto bool `json:"moto,omitempty"`

	// network
	Network string `json:"network,omitempty"`

	// three d secure
	ThreedSecure *StripeChargePaymentMethodDetailsCardThreeDSecure `json:"three_d_secure,omitempty"`

	// wallet
	Wallet *StripeChargePaymentMethodDetailsCardWallet `json:"wallet,omitempty"`
}

// Validate validates this stripe charge payment method details card
func (m *StripeChargePaymentMethodDetailsCard) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateChecks(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateInstallments(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateThreedSecure(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateWallet(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *StripeChargePaymentMethodDetailsCard) validateChecks(formats strfmt.Registry) error {

	if swag.IsZero(m.Checks) { // not required
		return nil
	}

	if m.Checks != nil {
		if err := m.Checks.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("checks")
			}
			return err
		}
	}

	return nil
}

func (m *StripeChargePaymentMethodDetailsCard) validateInstallments(formats strfmt.Registry) error {

	if swag.IsZero(m.Installments) { // not required
		return nil
	}

	if m.Installments != nil {
		if err := m.Installments.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("installments")
			}
			return err
		}
	}

	return nil
}

func (m *StripeChargePaymentMethodDetailsCard) validateThreedSecure(formats strfmt.Registry) error {

	if swag.IsZero(m.ThreedSecure) { // not required
		return nil
	}

	if m.ThreedSecure != nil {
		if err := m.ThreedSecure.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("three_d_secure")
			}
			return err
		}
	}

	return nil
}

func (m *StripeChargePaymentMethodDetailsCard) validateWallet(formats strfmt.Registry) error {

	if swag.IsZero(m.Wallet) { // not required
		return nil
	}

	if m.Wallet != nil {
		if err := m.Wallet.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("wallet")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (m *StripeChargePaymentMethodDetailsCard) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *StripeChargePaymentMethodDetailsCard) UnmarshalBinary(b []byte) error {
	var res StripeChargePaymentMethodDetailsCard
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
