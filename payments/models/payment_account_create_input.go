// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// PaymentAccountCreateInput payment account create input
//
// swagger:model payment_account.CreateInput
type PaymentAccountCreateInput struct {

	// payment method ID
	PaymentMethodID string `json:"paymentMethodID,omitempty"`
}

// Validate validates this payment account create input
func (m *PaymentAccountCreateInput) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *PaymentAccountCreateInput) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *PaymentAccountCreateInput) UnmarshalBinary(b []byte) error {
	var res PaymentAccountCreateInput
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
