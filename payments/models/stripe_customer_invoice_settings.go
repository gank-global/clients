// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"strconv"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// StripeCustomerInvoiceSettings stripe customer invoice settings
//
// swagger:model stripe.CustomerInvoiceSettings
type StripeCustomerInvoiceSettings struct {

	// custom fields
	CustomFields []*StripeCustomerInvoiceCustomField `json:"custom_fields"`

	// default payment method
	DefaultPaymentMethod *StripePaymentMethod `json:"default_payment_method,omitempty"`

	// footer
	Footer string `json:"footer,omitempty"`
}

// Validate validates this stripe customer invoice settings
func (m *StripeCustomerInvoiceSettings) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateCustomFields(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateDefaultPaymentMethod(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *StripeCustomerInvoiceSettings) validateCustomFields(formats strfmt.Registry) error {

	if swag.IsZero(m.CustomFields) { // not required
		return nil
	}

	for i := 0; i < len(m.CustomFields); i++ {
		if swag.IsZero(m.CustomFields[i]) { // not required
			continue
		}

		if m.CustomFields[i] != nil {
			if err := m.CustomFields[i].Validate(formats); err != nil {
				if ve, ok := err.(*errors.Validation); ok {
					return ve.ValidateName("custom_fields" + "." + strconv.Itoa(i))
				}
				return err
			}
		}

	}

	return nil
}

func (m *StripeCustomerInvoiceSettings) validateDefaultPaymentMethod(formats strfmt.Registry) error {

	if swag.IsZero(m.DefaultPaymentMethod) { // not required
		return nil
	}

	if m.DefaultPaymentMethod != nil {
		if err := m.DefaultPaymentMethod.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("default_payment_method")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (m *StripeCustomerInvoiceSettings) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *StripeCustomerInvoiceSettings) UnmarshalBinary(b []byte) error {
	var res StripeCustomerInvoiceSettings
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
