// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// StripePaymentIntentNextActionOXXODisplayDetails stripe payment intent next action o x x o display details
//
// swagger:model stripe.PaymentIntentNextActionOXXODisplayDetails
type StripePaymentIntentNextActionOXXODisplayDetails struct {

	// expires after
	ExpiresAfter int64 `json:"expires_after,omitempty"`

	// hosted voucher url
	HostedVoucherURL string `json:"hosted_voucher_url,omitempty"`

	// number
	Number string `json:"number,omitempty"`
}

// Validate validates this stripe payment intent next action o x x o display details
func (m *StripePaymentIntentNextActionOXXODisplayDetails) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *StripePaymentIntentNextActionOXXODisplayDetails) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *StripePaymentIntentNextActionOXXODisplayDetails) UnmarshalBinary(b []byte) error {
	var res StripePaymentIntentNextActionOXXODisplayDetails
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
