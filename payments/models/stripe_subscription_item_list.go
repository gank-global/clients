// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"strconv"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// StripeSubscriptionItemList stripe subscription item list
//
// swagger:model stripe.SubscriptionItemList
type StripeSubscriptionItemList struct {

	// data
	Data []*StripeSubscriptionItem `json:"data"`

	// has more
	HasMore bool `json:"has_more,omitempty"`

	// TotalCount is the total number of objects in the collection (beyond just
	// on the current page). This is not returned in most list calls.
	//
	// Deprecated: TotalCount is only included in some legacy situations and
	// not generally available anymore.
	TotalCount int64 `json:"total_count,omitempty"`

	// url
	URL string `json:"url,omitempty"`
}

// Validate validates this stripe subscription item list
func (m *StripeSubscriptionItemList) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateData(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *StripeSubscriptionItemList) validateData(formats strfmt.Registry) error {

	if swag.IsZero(m.Data) { // not required
		return nil
	}

	for i := 0; i < len(m.Data); i++ {
		if swag.IsZero(m.Data[i]) { // not required
			continue
		}

		if m.Data[i] != nil {
			if err := m.Data[i].Validate(formats); err != nil {
				if ve, ok := err.(*errors.Validation); ok {
					return ve.ValidateName("data" + "." + strconv.Itoa(i))
				}
				return err
			}
		}

	}

	return nil
}

// MarshalBinary interface implementation
func (m *StripeSubscriptionItemList) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *StripeSubscriptionItemList) UnmarshalBinary(b []byte) error {
	var res StripeSubscriptionItemList
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
