// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// DtoPaymentCheckoutResponse dto payment checkout response
//
// swagger:model dto.PaymentCheckoutResponse
type DtoPaymentCheckoutResponse struct {

	// client secret
	ClientSecret string `json:"clientSecret,omitempty"`

	// redirect Url
	RedirectURL string `json:"redirectUrl,omitempty"`

	// status
	Status string `json:"status,omitempty"`

	// token
	Token string `json:"token,omitempty"`

	HumanID string `json:"humanID"`
}

// Validate validates this dto payment checkout response
func (m *DtoPaymentCheckoutResponse) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *DtoPaymentCheckoutResponse) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *DtoPaymentCheckoutResponse) UnmarshalBinary(b []byte) error {
	var res DtoPaymentCheckoutResponse
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
