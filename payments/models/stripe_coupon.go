// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"github.com/go-openapi/errors"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// StripeCoupon stripe coupon
//
// swagger:model stripe.Coupon
type StripeCoupon struct {

	// amount off
	AmountOff int64 `json:"amount_off,omitempty"`

	// applies to
	AppliesTo *StripeCouponAppliesTo `json:"applies_to,omitempty"`

	// created
	Created int64 `json:"created,omitempty"`

	// currency
	Currency string `json:"currency,omitempty"`

	// deleted
	Deleted bool `json:"deleted,omitempty"`

	// duration
	Duration string `json:"duration,omitempty"`

	// duration in months
	DurationInMonths int64 `json:"duration_in_months,omitempty"`

	// id
	ID string `json:"id,omitempty"`

	// livemode
	Livemode bool `json:"livemode,omitempty"`

	// max redemptions
	MaxRedemptions int64 `json:"max_redemptions,omitempty"`

	// metadata
	Metadata map[string]string `json:"metadata,omitempty"`

	// name
	Name string `json:"name,omitempty"`

	// object
	Object string `json:"object,omitempty"`

	// percent off
	PercentOff float64 `json:"percent_off,omitempty"`

	// redeem by
	RedeemBy int64 `json:"redeem_by,omitempty"`

	// times redeemed
	TimesRedeemed int64 `json:"times_redeemed,omitempty"`

	// valid
	Valid bool `json:"valid,omitempty"`
}

// Validate validates this stripe coupon
func (m *StripeCoupon) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateAppliesTo(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *StripeCoupon) validateAppliesTo(formats strfmt.Registry) error {

	if swag.IsZero(m.AppliesTo) { // not required
		return nil
	}

	if m.AppliesTo != nil {
		if err := m.AppliesTo.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("applies_to")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (m *StripeCoupon) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *StripeCoupon) UnmarshalBinary(b []byte) error {
	var res StripeCoupon
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
