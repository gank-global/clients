// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"github.com/go-openapi/errors"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// StripeSetupAttemptPaymentMethodDetails stripe setup attempt payment method details
//
// swagger:model stripe.SetupAttemptPaymentMethodDetails
type StripeSetupAttemptPaymentMethodDetails struct {

	// bancontact
	Bancontact *StripeSetupAttemptPaymentMethodDetailsBancontact `json:"bancontact,omitempty"`

	// card
	Card *StripeSetupAttemptPaymentMethodDetailsCard `json:"card,omitempty"`

	// ideal
	Ideal *StripeSetupAttemptPaymentMethodDetailsIdeal `json:"ideal,omitempty"`

	// sofort
	Sofort *StripeSetupAttemptPaymentMethodDetailsSofort `json:"sofort,omitempty"`

	// type
	Type string `json:"type,omitempty"`
}

// Validate validates this stripe setup attempt payment method details
func (m *StripeSetupAttemptPaymentMethodDetails) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateBancontact(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateCard(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateIdeal(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateSofort(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *StripeSetupAttemptPaymentMethodDetails) validateBancontact(formats strfmt.Registry) error {

	if swag.IsZero(m.Bancontact) { // not required
		return nil
	}

	if m.Bancontact != nil {
		if err := m.Bancontact.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("bancontact")
			}
			return err
		}
	}

	return nil
}

func (m *StripeSetupAttemptPaymentMethodDetails) validateCard(formats strfmt.Registry) error {

	if swag.IsZero(m.Card) { // not required
		return nil
	}

	if m.Card != nil {
		if err := m.Card.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("card")
			}
			return err
		}
	}

	return nil
}

func (m *StripeSetupAttemptPaymentMethodDetails) validateIdeal(formats strfmt.Registry) error {

	if swag.IsZero(m.Ideal) { // not required
		return nil
	}

	if m.Ideal != nil {
		if err := m.Ideal.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("ideal")
			}
			return err
		}
	}

	return nil
}

func (m *StripeSetupAttemptPaymentMethodDetails) validateSofort(formats strfmt.Registry) error {

	if swag.IsZero(m.Sofort) { // not required
		return nil
	}

	if m.Sofort != nil {
		if err := m.Sofort.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("sofort")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (m *StripeSetupAttemptPaymentMethodDetails) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *StripeSetupAttemptPaymentMethodDetails) UnmarshalBinary(b []byte) error {
	var res StripeSetupAttemptPaymentMethodDetails
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
