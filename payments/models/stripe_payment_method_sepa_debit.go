// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"github.com/go-openapi/errors"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// StripePaymentMethodSepaDebit stripe payment method sepa debit
//
// swagger:model stripe.PaymentMethodSepaDebit
type StripePaymentMethodSepaDebit struct {

	// bank code
	BankCode string `json:"bank_code,omitempty"`

	// branch code
	BranchCode string `json:"branch_code,omitempty"`

	// country
	Country string `json:"country,omitempty"`

	// fingerprint
	Fingerprint string `json:"fingerprint,omitempty"`

	// generated from
	GeneratedFrom *StripePaymentMethodSepaDebitGeneratedFrom `json:"generated_from,omitempty"`

	// last4
	Last4 string `json:"last4,omitempty"`
}

// Validate validates this stripe payment method sepa debit
func (m *StripePaymentMethodSepaDebit) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateGeneratedFrom(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *StripePaymentMethodSepaDebit) validateGeneratedFrom(formats strfmt.Registry) error {

	if swag.IsZero(m.GeneratedFrom) { // not required
		return nil
	}

	if m.GeneratedFrom != nil {
		if err := m.GeneratedFrom.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("generated_from")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (m *StripePaymentMethodSepaDebit) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *StripePaymentMethodSepaDebit) UnmarshalBinary(b []byte) error {
	var res StripePaymentMethodSepaDebit
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
