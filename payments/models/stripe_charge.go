// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"github.com/go-openapi/errors"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// StripeCharge stripe charge
//
// swagger:model stripe.Charge
type StripeCharge struct {

	// amount
	Amount int64 `json:"amount,omitempty"`

	// amount captured
	AmountCaptured int64 `json:"amount_captured,omitempty"`

	// amount refunded
	AmountRefunded int64 `json:"amount_refunded,omitempty"`

	// application
	Application *StripeApplication `json:"application,omitempty"`

	// application fee
	ApplicationFee *StripeApplicationFee `json:"application_fee,omitempty"`

	// application fee amount
	ApplicationFeeAmount int64 `json:"application_fee_amount,omitempty"`

	// authorization code
	AuthorizationCode string `json:"authorization_code,omitempty"`

	// balance transaction
	BalanceTransaction *StripeBalanceTransaction `json:"balance_transaction,omitempty"`

	// billing details
	BillingDetails *StripeBillingDetails `json:"billing_details,omitempty"`

	// calculated statement descriptor
	CalculatedStatementDescriptor string `json:"calculated_statement_descriptor,omitempty"`

	// captured
	Captured bool `json:"captured,omitempty"`

	// created
	Created int64 `json:"created,omitempty"`

	// currency
	Currency string `json:"currency,omitempty"`

	// customer
	Customer *StripeCustomer `json:"customer,omitempty"`

	// description
	Description string `json:"description,omitempty"`

	// destination
	Destination *StripeAccount `json:"destination,omitempty"`

	// dispute
	Dispute *StripeDispute `json:"dispute,omitempty"`

	// disputed
	Disputed bool `json:"disputed,omitempty"`

	// failure code
	FailureCode string `json:"failure_code,omitempty"`

	// failure message
	FailureMessage string `json:"failure_message,omitempty"`

	// fraud details
	FraudDetails *StripeFraudDetails `json:"fraud_details,omitempty"`

	// id
	ID string `json:"id,omitempty"`

	// invoice
	Invoice *StripeInvoice `json:"invoice,omitempty"`

	// level3
	Level3 *StripeChargeLevel3 `json:"level3,omitempty"`

	// livemode
	Livemode bool `json:"livemode,omitempty"`

	// metadata
	Metadata map[string]string `json:"metadata,omitempty"`

	// on behalf of
	OnBehalfOf *StripeAccount `json:"on_behalf_of,omitempty"`

	// outcome
	Outcome *StripeChargeOutcome `json:"outcome,omitempty"`

	// paid
	Paid bool `json:"paid,omitempty"`

	// payment intent
	PaymentIntent *StripePaymentIntent `json:"payment_intent,omitempty"`

	// payment method
	PaymentMethod string `json:"payment_method,omitempty"`

	// payment method details
	PaymentMethodDetails *StripeChargePaymentMethodDetails `json:"payment_method_details,omitempty"`

	// receipt email
	ReceiptEmail string `json:"receipt_email,omitempty"`

	// receipt number
	ReceiptNumber string `json:"receipt_number,omitempty"`

	// receipt url
	ReceiptURL string `json:"receipt_url,omitempty"`

	// refunded
	Refunded bool `json:"refunded,omitempty"`

	// refunds
	Refunds *StripeRefundList `json:"refunds,omitempty"`

	// review
	Review *StripeReview `json:"review,omitempty"`

	// shipping
	Shipping *StripeShippingDetails `json:"shipping,omitempty"`

	// source
	Source *StripePaymentSource `json:"source,omitempty"`

	// source transfer
	SourceTransfer *StripeTransfer `json:"source_transfer,omitempty"`

	// statement descriptor
	StatementDescriptor string `json:"statement_descriptor,omitempty"`

	// statement descriptor suffix
	StatementDescriptorSuffix string `json:"statement_descriptor_suffix,omitempty"`

	// status
	Status string `json:"status,omitempty"`

	// transfer
	Transfer *StripeTransfer `json:"transfer,omitempty"`

	// transfer data
	TransferData *StripeChargeTransferData `json:"transfer_data,omitempty"`

	// transfer group
	TransferGroup string `json:"transfer_group,omitempty"`
}

// Validate validates this stripe charge
func (m *StripeCharge) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateApplication(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateApplicationFee(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateBalanceTransaction(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateBillingDetails(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateCustomer(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateDestination(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateDispute(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateFraudDetails(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateInvoice(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateLevel3(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateOnBehalfOf(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateOutcome(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validatePaymentIntent(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validatePaymentMethodDetails(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateRefunds(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateReview(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateShipping(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateSource(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateSourceTransfer(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateTransfer(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateTransferData(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *StripeCharge) validateApplication(formats strfmt.Registry) error {

	if swag.IsZero(m.Application) { // not required
		return nil
	}

	if m.Application != nil {
		if err := m.Application.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("application")
			}
			return err
		}
	}

	return nil
}

func (m *StripeCharge) validateApplicationFee(formats strfmt.Registry) error {

	if swag.IsZero(m.ApplicationFee) { // not required
		return nil
	}

	if m.ApplicationFee != nil {
		if err := m.ApplicationFee.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("application_fee")
			}
			return err
		}
	}

	return nil
}

func (m *StripeCharge) validateBalanceTransaction(formats strfmt.Registry) error {

	if swag.IsZero(m.BalanceTransaction) { // not required
		return nil
	}

	if m.BalanceTransaction != nil {
		if err := m.BalanceTransaction.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("balance_transaction")
			}
			return err
		}
	}

	return nil
}

func (m *StripeCharge) validateBillingDetails(formats strfmt.Registry) error {

	if swag.IsZero(m.BillingDetails) { // not required
		return nil
	}

	if m.BillingDetails != nil {
		if err := m.BillingDetails.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("billing_details")
			}
			return err
		}
	}

	return nil
}

func (m *StripeCharge) validateCustomer(formats strfmt.Registry) error {

	if swag.IsZero(m.Customer) { // not required
		return nil
	}

	if m.Customer != nil {
		if err := m.Customer.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("customer")
			}
			return err
		}
	}

	return nil
}

func (m *StripeCharge) validateDestination(formats strfmt.Registry) error {

	if swag.IsZero(m.Destination) { // not required
		return nil
	}

	if m.Destination != nil {
		if err := m.Destination.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("destination")
			}
			return err
		}
	}

	return nil
}

func (m *StripeCharge) validateDispute(formats strfmt.Registry) error {

	if swag.IsZero(m.Dispute) { // not required
		return nil
	}

	if m.Dispute != nil {
		if err := m.Dispute.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("dispute")
			}
			return err
		}
	}

	return nil
}

func (m *StripeCharge) validateFraudDetails(formats strfmt.Registry) error {

	if swag.IsZero(m.FraudDetails) { // not required
		return nil
	}

	if m.FraudDetails != nil {
		if err := m.FraudDetails.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("fraud_details")
			}
			return err
		}
	}

	return nil
}

func (m *StripeCharge) validateInvoice(formats strfmt.Registry) error {

	if swag.IsZero(m.Invoice) { // not required
		return nil
	}

	if m.Invoice != nil {
		if err := m.Invoice.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("invoice")
			}
			return err
		}
	}

	return nil
}

func (m *StripeCharge) validateLevel3(formats strfmt.Registry) error {

	if swag.IsZero(m.Level3) { // not required
		return nil
	}

	if m.Level3 != nil {
		if err := m.Level3.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("level3")
			}
			return err
		}
	}

	return nil
}

func (m *StripeCharge) validateOnBehalfOf(formats strfmt.Registry) error {

	if swag.IsZero(m.OnBehalfOf) { // not required
		return nil
	}

	if m.OnBehalfOf != nil {
		if err := m.OnBehalfOf.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("on_behalf_of")
			}
			return err
		}
	}

	return nil
}

func (m *StripeCharge) validateOutcome(formats strfmt.Registry) error {

	if swag.IsZero(m.Outcome) { // not required
		return nil
	}

	if m.Outcome != nil {
		if err := m.Outcome.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("outcome")
			}
			return err
		}
	}

	return nil
}

func (m *StripeCharge) validatePaymentIntent(formats strfmt.Registry) error {

	if swag.IsZero(m.PaymentIntent) { // not required
		return nil
	}

	if m.PaymentIntent != nil {
		if err := m.PaymentIntent.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("payment_intent")
			}
			return err
		}
	}

	return nil
}

func (m *StripeCharge) validatePaymentMethodDetails(formats strfmt.Registry) error {

	if swag.IsZero(m.PaymentMethodDetails) { // not required
		return nil
	}

	if m.PaymentMethodDetails != nil {
		if err := m.PaymentMethodDetails.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("payment_method_details")
			}
			return err
		}
	}

	return nil
}

func (m *StripeCharge) validateRefunds(formats strfmt.Registry) error {

	if swag.IsZero(m.Refunds) { // not required
		return nil
	}

	if m.Refunds != nil {
		if err := m.Refunds.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("refunds")
			}
			return err
		}
	}

	return nil
}

func (m *StripeCharge) validateReview(formats strfmt.Registry) error {

	if swag.IsZero(m.Review) { // not required
		return nil
	}

	if m.Review != nil {
		if err := m.Review.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("review")
			}
			return err
		}
	}

	return nil
}

func (m *StripeCharge) validateShipping(formats strfmt.Registry) error {

	if swag.IsZero(m.Shipping) { // not required
		return nil
	}

	if m.Shipping != nil {
		if err := m.Shipping.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("shipping")
			}
			return err
		}
	}

	return nil
}

func (m *StripeCharge) validateSource(formats strfmt.Registry) error {

	if swag.IsZero(m.Source) { // not required
		return nil
	}

	if m.Source != nil {
		if err := m.Source.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("source")
			}
			return err
		}
	}

	return nil
}

func (m *StripeCharge) validateSourceTransfer(formats strfmt.Registry) error {

	if swag.IsZero(m.SourceTransfer) { // not required
		return nil
	}

	if m.SourceTransfer != nil {
		if err := m.SourceTransfer.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("source_transfer")
			}
			return err
		}
	}

	return nil
}

func (m *StripeCharge) validateTransfer(formats strfmt.Registry) error {

	if swag.IsZero(m.Transfer) { // not required
		return nil
	}

	if m.Transfer != nil {
		if err := m.Transfer.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("transfer")
			}
			return err
		}
	}

	return nil
}

func (m *StripeCharge) validateTransferData(formats strfmt.Registry) error {

	if swag.IsZero(m.TransferData) { // not required
		return nil
	}

	if m.TransferData != nil {
		if err := m.TransferData.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("transfer_data")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (m *StripeCharge) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *StripeCharge) UnmarshalBinary(b []byte) error {
	var res StripeCharge
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
