// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"strconv"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// StripeBalanceTransaction stripe balance transaction
//
// swagger:model stripe.BalanceTransaction
type StripeBalanceTransaction struct {

	// amount
	Amount int64 `json:"amount,omitempty"`

	// available on
	AvailableOn int64 `json:"available_on,omitempty"`

	// created
	Created int64 `json:"created,omitempty"`

	// currency
	Currency string `json:"currency,omitempty"`

	// description
	Description string `json:"description,omitempty"`

	// exchange rate
	ExchangeRate float64 `json:"exchange_rate,omitempty"`

	// fee
	Fee int64 `json:"fee,omitempty"`

	// fee details
	FeeDetails []*StripeBalanceTransactionFee `json:"fee_details"`

	// id
	ID string `json:"id,omitempty"`

	// net
	Net int64 `json:"net,omitempty"`

	// reporting category
	ReportingCategory string `json:"reporting_category,omitempty"`

	// source
	Source *StripeBalanceTransactionSource `json:"source,omitempty"`

	// status
	Status string `json:"status,omitempty"`

	// type
	Type string `json:"type,omitempty"`
}

// Validate validates this stripe balance transaction
func (m *StripeBalanceTransaction) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateFeeDetails(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateSource(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *StripeBalanceTransaction) validateFeeDetails(formats strfmt.Registry) error {

	if swag.IsZero(m.FeeDetails) { // not required
		return nil
	}

	for i := 0; i < len(m.FeeDetails); i++ {
		if swag.IsZero(m.FeeDetails[i]) { // not required
			continue
		}

		if m.FeeDetails[i] != nil {
			if err := m.FeeDetails[i].Validate(formats); err != nil {
				if ve, ok := err.(*errors.Validation); ok {
					return ve.ValidateName("fee_details" + "." + strconv.Itoa(i))
				}
				return err
			}
		}

	}

	return nil
}

func (m *StripeBalanceTransaction) validateSource(formats strfmt.Registry) error {

	if swag.IsZero(m.Source) { // not required
		return nil
	}

	if m.Source != nil {
		if err := m.Source.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("source")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (m *StripeBalanceTransaction) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *StripeBalanceTransaction) UnmarshalBinary(b []byte) error {
	var res StripeBalanceTransaction
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
