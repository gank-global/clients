// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// StripePriceTier stripe price tier
//
// swagger:model stripe.PriceTier
type StripePriceTier struct {

	// flat amount
	FlatAmount int64 `json:"flat_amount,omitempty"`

	// flat amount decimal
	FlatAmountDecimal string `json:"flat_amount_decimal,omitempty"`

	// unit amount
	UnitAmount int64 `json:"unit_amount,omitempty"`

	// unit amount decimal
	UnitAmountDecimal string `json:"unit_amount_decimal,omitempty"`

	// up to
	UpTo int64 `json:"up_to,omitempty"`
}

// Validate validates this stripe price tier
func (m *StripePriceTier) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *StripePriceTier) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *StripePriceTier) UnmarshalBinary(b []byte) error {
	var res StripePriceTier
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
