// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// StripeSubscriptionPendingInvoiceItemInterval stripe subscription pending invoice item interval
//
// swagger:model stripe.SubscriptionPendingInvoiceItemInterval
type StripeSubscriptionPendingInvoiceItemInterval struct {

	// interval
	Interval string `json:"interval,omitempty"`

	// interval count
	IntervalCount int64 `json:"interval_count,omitempty"`
}

// Validate validates this stripe subscription pending invoice item interval
func (m *StripeSubscriptionPendingInvoiceItemInterval) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *StripeSubscriptionPendingInvoiceItemInterval) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *StripeSubscriptionPendingInvoiceItemInterval) UnmarshalBinary(b []byte) error {
	var res StripeSubscriptionPendingInvoiceItemInterval
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
