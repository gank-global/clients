// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"

	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// DtoPaymentSubscriptionInput dto payment subscription input
//
// swagger:model dto.PaymentSubscriptionInput
type DtoPaymentSubscriptionInput struct {

	// country code
	CountryCode string `json:"countryCode,omitempty"`

	// currency
	Currency string `json:"currency,omitempty"`

	// currency for stripe
	CurrencyForStripe string `json:"currencyForStripe,omitempty"`

	// interval
	Interval string `json:"interval,omitempty"`

	// interval count
	IntervalCount int64 `json:"intervalCount,omitempty"`

	// metadata
	Metadata map[string]string `json:"metadata,omitempty"`

	// month interval
	MonthInterval int64 `json:"monthInterval,omitempty"`

	// order ID
	OrderID string `json:"orderID,omitempty"`

	// order number
	OrderNumber string `json:"orderNumber,omitempty"`

	// payment method ID
	PaymentMethodID string `json:"paymentMethodID,omitempty"`

	// payment type
	PaymentType string `json:"paymentType,omitempty"`

	// total amount
	TotalAmount float64 `json:"totalAmount,omitempty"`

	// total amount for stripe
	TotalAmountForStripe float64 `json:"totalAmountForStripe,omitempty"`

	TotalAmountProcessingFee *float64 `json:"totalAmountProcessingFee,omitempty"`
	TotalAmountProcessingConversionFee *float64 `json:"totalAmountProcessingConversionFee"`
	TotalAmountProcessingAppliedFee *float64 `json:"totalAmountProcessingAppliedFee"`
	TotalAmountProcessingTaxedFee *float64 `json:"totalAmountProcessingTaxedFee"`

	PaymentMethodData	map[string]interface{} `json:"paymentMethodData"`

	// user ID
	UserID string `json:"userID,omitempty"`

	Usecase *UsecaseV2 `json:"usecase"`
}

// Validate validates this dto payment subscription input
func (m *DtoPaymentSubscriptionInput) Validate(formats strfmt.Registry) error {
	return nil
}

// ContextValidate validates this dto payment subscription input based on context it is used
func (m *DtoPaymentSubscriptionInput) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *DtoPaymentSubscriptionInput) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *DtoPaymentSubscriptionInput) UnmarshalBinary(b []byte) error {
	var res DtoPaymentSubscriptionInput
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
