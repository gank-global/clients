// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"github.com/go-openapi/errors"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// StripeAccountCompany stripe account company
//
// swagger:model stripe.AccountCompany
type StripeAccountCompany struct {

	// address
	Address *StripeAccountAddress `json:"address,omitempty"`

	// address kana
	AddressKana *StripeAccountAddress `json:"address_kana,omitempty"`

	// address kanji
	AddressKanji *StripeAccountAddress `json:"address_kanji,omitempty"`

	// directors provided
	DirectorsProvided bool `json:"directors_provided,omitempty"`

	// executives provided
	ExecutivesProvided bool `json:"executives_provided,omitempty"`

	// name
	Name string `json:"name,omitempty"`

	// name kana
	NameKana string `json:"name_kana,omitempty"`

	// name kanji
	NameKanji string `json:"name_kanji,omitempty"`

	// owners provided
	OwnersProvided bool `json:"owners_provided,omitempty"`

	// phone
	Phone string `json:"phone,omitempty"`

	// registration number
	RegistrationNumber string `json:"registration_number,omitempty"`

	// structure
	Structure string `json:"structure,omitempty"`

	// tax id provided
	TaxIDProvided bool `json:"tax_id_provided,omitempty"`

	// tax id registrar
	TaxIDRegistrar string `json:"tax_id_registrar,omitempty"`

	// vat id provided
	VatIDProvided bool `json:"vat_id_provided,omitempty"`

	// verification
	Verification *StripeAccountCompanyVerification `json:"verification,omitempty"`
}

// Validate validates this stripe account company
func (m *StripeAccountCompany) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateAddress(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateAddressKana(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateAddressKanji(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateVerification(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *StripeAccountCompany) validateAddress(formats strfmt.Registry) error {

	if swag.IsZero(m.Address) { // not required
		return nil
	}

	if m.Address != nil {
		if err := m.Address.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("address")
			}
			return err
		}
	}

	return nil
}

func (m *StripeAccountCompany) validateAddressKana(formats strfmt.Registry) error {

	if swag.IsZero(m.AddressKana) { // not required
		return nil
	}

	if m.AddressKana != nil {
		if err := m.AddressKana.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("address_kana")
			}
			return err
		}
	}

	return nil
}

func (m *StripeAccountCompany) validateAddressKanji(formats strfmt.Registry) error {

	if swag.IsZero(m.AddressKanji) { // not required
		return nil
	}

	if m.AddressKanji != nil {
		if err := m.AddressKanji.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("address_kanji")
			}
			return err
		}
	}

	return nil
}

func (m *StripeAccountCompany) validateVerification(formats strfmt.Registry) error {

	if swag.IsZero(m.Verification) { // not required
		return nil
	}

	if m.Verification != nil {
		if err := m.Verification.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("verification")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (m *StripeAccountCompany) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *StripeAccountCompany) UnmarshalBinary(b []byte) error {
	var res StripeAccountCompany
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
