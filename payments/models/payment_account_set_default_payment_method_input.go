// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// PaymentAccountSetDefaultPaymentMethodInput payment account set default payment method input
//
// swagger:model payment_account.SetDefaultPaymentMethodInput
type PaymentAccountSetDefaultPaymentMethodInput struct {

	// default payment method
	DefaultPaymentMethod string `json:"defaultPaymentMethod,omitempty"`
}

// Validate validates this payment account set default payment method input
func (m *PaymentAccountSetDefaultPaymentMethodInput) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *PaymentAccountSetDefaultPaymentMethodInput) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *PaymentAccountSetDefaultPaymentMethodInput) UnmarshalBinary(b []byte) error {
	var res PaymentAccountSetDefaultPaymentMethodInput
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
