// Code generated by go-swagger; DO NOT EDIT.

package catalogs

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"fmt"
	"io"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"

	"bitbucket.org/gank-global/clients/catalogs/models"
)

// CreateMediaReader is a Reader for the CreateMedia structure.
type CreateMediaReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *CreateMediaReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 200:
		result := NewCreateMediaOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		result := NewResponseError()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result
	}
}

// NewCreateMediaOK creates a CreateMediaOK with default headers values
func NewCreateMediaOK() *CreateMediaOK {
	return &CreateMediaOK{}
}

/*CreateMediaOK handles this case with default header values.

OK
*/
type CreateMediaOK struct {
	Payload *CreateMediaOKBody
}

func (o *CreateMediaOK) Error() string {
	return fmt.Sprintf("[POST /catalogs/services/{id}/media][%d] createMediaOK  %+v", 200, o.Payload)
}

func (o *CreateMediaOK) GetPayload() *CreateMediaOKBody {
	return o.Payload
}

func (o *CreateMediaOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(CreateMediaOKBody)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

/*CreateMediaBadRequestBody create media bad request body
swagger:model CreateMediaBadRequestBody
*/

type CreateMediaBadRequestBody struct {

	// errors
	Errors *models.ErrorResponse `json:"errors,omitempty"`
}

// Validate validates this create media bad request body
func (o *CreateMediaBadRequestBody) Validate(formats strfmt.Registry) error {
	var res []error

	if err := o.validateErrors(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *CreateMediaBadRequestBody) validateErrors(formats strfmt.Registry) error {
	if swag.IsZero(o.Errors) { // not required
		return nil
	}

	if o.Errors != nil {
		if err := o.Errors.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("createMediaBadRequest" + "." + "errors")
			}
			return err
		}
	}

	return nil
}

// ContextValidate validate this create media bad request body based on the context it is used
func (o *CreateMediaBadRequestBody) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	var res []error

	if err := o.contextValidateErrors(ctx, formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *CreateMediaBadRequestBody) contextValidateErrors(ctx context.Context, formats strfmt.Registry) error {

	if o.Errors != nil {
		if err := o.Errors.ContextValidate(ctx, formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("createMediaBadRequest" + "." + "errors")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (o *CreateMediaBadRequestBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *CreateMediaBadRequestBody) UnmarshalBinary(b []byte) error {
	var res CreateMediaBadRequestBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}

/*CreateMediaInternalServerErrorBody create media internal server error body
swagger:model CreateMediaInternalServerErrorBody
*/

type CreateMediaInternalServerErrorBody struct {

	// errors
	Errors *models.ErrorResponse `json:"errors,omitempty"`
}

// Validate validates this create media internal server error body
func (o *CreateMediaInternalServerErrorBody) Validate(formats strfmt.Registry) error {
	var res []error

	if err := o.validateErrors(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *CreateMediaInternalServerErrorBody) validateErrors(formats strfmt.Registry) error {
	if swag.IsZero(o.Errors) { // not required
		return nil
	}

	if o.Errors != nil {
		if err := o.Errors.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("createMediaInternalServerError" + "." + "errors")
			}
			return err
		}
	}

	return nil
}

// ContextValidate validate this create media internal server error body based on the context it is used
func (o *CreateMediaInternalServerErrorBody) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	var res []error

	if err := o.contextValidateErrors(ctx, formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *CreateMediaInternalServerErrorBody) contextValidateErrors(ctx context.Context, formats strfmt.Registry) error {

	if o.Errors != nil {
		if err := o.Errors.ContextValidate(ctx, formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("createMediaInternalServerError" + "." + "errors")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (o *CreateMediaInternalServerErrorBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *CreateMediaInternalServerErrorBody) UnmarshalBinary(b []byte) error {
	var res CreateMediaInternalServerErrorBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}

/*CreateMediaOKBody create media o k body
swagger:model CreateMediaOKBody
*/

type CreateMediaOKBody struct {

	// data
	Data *models.Media `json:"data,omitempty"`
}

// Validate validates this create media o k body
func (o *CreateMediaOKBody) Validate(formats strfmt.Registry) error {
	var res []error

	if err := o.validateData(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *CreateMediaOKBody) validateData(formats strfmt.Registry) error {
	if swag.IsZero(o.Data) { // not required
		return nil
	}

	if o.Data != nil {
		if err := o.Data.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("createMediaOK" + "." + "data")
			}
			return err
		}
	}

	return nil
}

// ContextValidate validate this create media o k body based on the context it is used
func (o *CreateMediaOKBody) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	var res []error

	if err := o.contextValidateData(ctx, formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *CreateMediaOKBody) contextValidateData(ctx context.Context, formats strfmt.Registry) error {

	if o.Data != nil {
		if err := o.Data.ContextValidate(ctx, formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("createMediaOK" + "." + "data")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (o *CreateMediaOKBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *CreateMediaOKBody) UnmarshalBinary(b []byte) error {
	var res CreateMediaOKBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}

/*CreateMediaUnprocessableEntityBody create media unprocessable entity body
swagger:model CreateMediaUnprocessableEntityBody
*/

type CreateMediaUnprocessableEntityBody struct {

	// errors
	Errors *models.ErrorResponse `json:"errors,omitempty"`
}

// Validate validates this create media unprocessable entity body
func (o *CreateMediaUnprocessableEntityBody) Validate(formats strfmt.Registry) error {
	var res []error

	if err := o.validateErrors(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *CreateMediaUnprocessableEntityBody) validateErrors(formats strfmt.Registry) error {
	if swag.IsZero(o.Errors) { // not required
		return nil
	}

	if o.Errors != nil {
		if err := o.Errors.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("createMediaUnprocessableEntity" + "." + "errors")
			}
			return err
		}
	}

	return nil
}

// ContextValidate validate this create media unprocessable entity body based on the context it is used
func (o *CreateMediaUnprocessableEntityBody) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	var res []error

	if err := o.contextValidateErrors(ctx, formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *CreateMediaUnprocessableEntityBody) contextValidateErrors(ctx context.Context, formats strfmt.Registry) error {

	if o.Errors != nil {
		if err := o.Errors.ContextValidate(ctx, formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("createMediaUnprocessableEntity" + "." + "errors")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (o *CreateMediaUnprocessableEntityBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *CreateMediaUnprocessableEntityBody) UnmarshalBinary(b []byte) error {
	var res CreateMediaUnprocessableEntityBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}
