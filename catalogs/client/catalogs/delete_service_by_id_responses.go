// Code generated by go-swagger; DO NOT EDIT.

package catalogs

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"
)

// DeleteServiceByIDReader is a Reader for the DeleteServiceByID structure.
type DeleteServiceByIDReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *DeleteServiceByIDReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 200:
		result := NewDeleteServiceByIDOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		result := NewResponseError()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result
	}
}

// NewDeleteServiceByIDOK creates a DeleteServiceByIDOK with default headers values
func NewDeleteServiceByIDOK() *DeleteServiceByIDOK {
	return &DeleteServiceByIDOK{}
}

/*DeleteServiceByIDOK handles this case with default header values.

OK
*/
type DeleteServiceByIDOK struct {
}

func (o *DeleteServiceByIDOK) Error() string {
	return fmt.Sprintf("[DELETE /catalogs/services/{id}][%d] deleteServiceByIdOK ", 200)
}

func (o *DeleteServiceByIDOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}
