// Code generated by go-swagger; DO NOT EDIT.

package catalogs

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"
)

// DeleteCategoryByIDReader is a Reader for the DeleteCategoryByID structure.
type DeleteCategoryByIDReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *DeleteCategoryByIDReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 200:
		result := NewDeleteCategoryByIDOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		result := NewResponseError()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result
	}
}

// NewDeleteCategoryByIDOK creates a DeleteCategoryByIDOK with default headers values
func NewDeleteCategoryByIDOK() *DeleteCategoryByIDOK {
	return &DeleteCategoryByIDOK{}
}

/*DeleteCategoryByIDOK handles this case with default header values.

OK
*/
type DeleteCategoryByIDOK struct {
}

func (o *DeleteCategoryByIDOK) Error() string {
	return fmt.Sprintf("[DELETE /catalogs/categories/{id}][%d] deleteCategoryByIdOK ", 200)
}

func (o *DeleteCategoryByIDOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}
