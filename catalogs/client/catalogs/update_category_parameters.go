// Code generated by go-swagger; DO NOT EDIT.

package catalogs

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"

	"bitbucket.org/gank-global/clients/catalogs/models"
)

// NewUpdateCategoryParams creates a new UpdateCategoryParams object
// with the default values initialized.
func NewUpdateCategoryParams() *UpdateCategoryParams {
	var ()
	return &UpdateCategoryParams{

		Timeout: cr.DefaultTimeout,
	}
}

// NewUpdateCategoryParamsWithTimeout creates a new UpdateCategoryParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewUpdateCategoryParamsWithTimeout(timeout time.Duration) *UpdateCategoryParams {
	var ()
	return &UpdateCategoryParams{

		Timeout: timeout,
	}
}

// NewUpdateCategoryParamsWithContext creates a new UpdateCategoryParams object
// with the default values initialized, and the ability to set a context for a request
func NewUpdateCategoryParamsWithContext(ctx context.Context) *UpdateCategoryParams {
	var ()
	return &UpdateCategoryParams{

		Context: ctx,
	}
}

// NewUpdateCategoryParamsWithHTTPClient creates a new UpdateCategoryParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewUpdateCategoryParamsWithHTTPClient(client *http.Client) *UpdateCategoryParams {
	var ()
	return &UpdateCategoryParams{
		HTTPClient: client,
	}
}

/*UpdateCategoryParams contains all the parameters to send to the API endpoint
for the update category operation typically these are written to a http.Request
*/
type UpdateCategoryParams struct {

	/*Content
	  Category payload

	*/
	Content *models.CategoryUpdateInput
	/*ID
	  Category ID

	*/
	ID string

	Timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
	SMUserID   string
}

// WithTimeout adds the timeout to the update category params
func (o *UpdateCategoryParams) WithTimeout(timeout time.Duration) *UpdateCategoryParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the update category params
func (o *UpdateCategoryParams) SetTimeout(timeout time.Duration) {
	o.Timeout = timeout
}

// WithContext adds the context to the update category params
func (o *UpdateCategoryParams) WithContext(ctx context.Context) *UpdateCategoryParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the update category params
func (o *UpdateCategoryParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the update category params
func (o *UpdateCategoryParams) WithHTTPClient(client *http.Client) *UpdateCategoryParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the update category params
func (o *UpdateCategoryParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithContent adds the content to the update category params
func (o *UpdateCategoryParams) WithContent(content *models.CategoryUpdateInput) *UpdateCategoryParams {
	o.SetContent(content)
	return o
}

// SetContent adds the content to the update category params
func (o *UpdateCategoryParams) SetContent(content *models.CategoryUpdateInput) {
	o.Content = content
}

// WithID adds the id to the update category params
func (o *UpdateCategoryParams) WithID(id string) *UpdateCategoryParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the update category params
func (o *UpdateCategoryParams) SetID(id string) {
	o.ID = id
}

// WriteToRequest writes these params to a swagger request
func (o *UpdateCategoryParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {
	// header param ID
	if o.SMUserID != "" {
		if err := r.SetHeaderParam("X-Sm-User-ID", o.SMUserID); err != nil {
			return err
		}
	}
	if err := r.SetTimeout(o.Timeout); err != nil {
		return err
	}
	var res []error

	if o.Content != nil {
		if err := r.SetBodyParam(o.Content); err != nil {
			return err
		}
	}

	// path param id
	if err := r.SetPathParam("id", o.ID); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
