// Code generated by go-swagger; DO NOT EDIT.

package media

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"fmt"
	"io"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"

	"bitbucket.org/gank-global/clients/media/models"
)

// MediaPostDigitalGoodsReader is a Reader for the MediaPostDigitalGoods structure.
type MediaPostDigitalGoodsReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *MediaPostDigitalGoodsReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 200:
		result := NewMediaPostDigitalGoodsOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		result := NewResponseError()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result
	}
}

// NewMediaPostDigitalGoodsOK creates a MediaPostDigitalGoodsOK with default headers values
func NewMediaPostDigitalGoodsOK() *MediaPostDigitalGoodsOK {
	return &MediaPostDigitalGoodsOK{}
}

/*MediaPostDigitalGoodsOK handles this case with default header values.

Ok
*/
type MediaPostDigitalGoodsOK struct {
	Payload *MediaPostDigitalGoodsOKBody
}

func (o *MediaPostDigitalGoodsOK) Error() string {
	return fmt.Sprintf("[POST /media/digital-goods][%d] mediaPostDigitalGoodsOK  %+v", 200, o.Payload)
}

func (o *MediaPostDigitalGoodsOK) GetPayload() *MediaPostDigitalGoodsOKBody {
	return o.Payload
}

func (o *MediaPostDigitalGoodsOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(MediaPostDigitalGoodsOKBody)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

/*MediaPostDigitalGoodsOKBody media post digital goods o k body
swagger:model MediaPostDigitalGoodsOKBody
*/

type MediaPostDigitalGoodsOKBody struct {

	// data
	Data *models.Media `json:"data,omitempty"`
}

// Validate validates this media post digital goods o k body
func (o *MediaPostDigitalGoodsOKBody) Validate(formats strfmt.Registry) error {
	var res []error

	if err := o.validateData(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *MediaPostDigitalGoodsOKBody) validateData(formats strfmt.Registry) error {
	if swag.IsZero(o.Data) { // not required
		return nil
	}

	if o.Data != nil {
		if err := o.Data.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("mediaPostDigitalGoodsOK" + "." + "data")
			}
			return err
		}
	}

	return nil
}

// ContextValidate validate this media post digital goods o k body based on the context it is used
func (o *MediaPostDigitalGoodsOKBody) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	var res []error

	if err := o.contextValidateData(ctx, formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *MediaPostDigitalGoodsOKBody) contextValidateData(ctx context.Context, formats strfmt.Registry) error {

	if o.Data != nil {
		if err := o.Data.ContextValidate(ctx, formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("mediaPostDigitalGoodsOK" + "." + "data")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (o *MediaPostDigitalGoodsOKBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *MediaPostDigitalGoodsOKBody) UnmarshalBinary(b []byte) error {
	var res MediaPostDigitalGoodsOKBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}
