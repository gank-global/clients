// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// ServiceGame service game
//
// swagger:model ServiceGame
type ServiceGame struct {

	// description
	Description string `json:"description,omitempty"`

	// icon
	Icon string `json:"icon,omitempty"`

	// id
	ID string `json:"id,omitempty"`

	// image
	Image string `json:"image,omitempty"`

	// is active
	IsActive bool `json:"isActive,omitempty"`

	// name
	Name string `json:"name,omitempty"`

	// sort
	Sort int64 `json:"sort,omitempty"`
}

// Validate validates this service game
func (m *ServiceGame) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *ServiceGame) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *ServiceGame) UnmarshalBinary(b []byte) error {
	var res ServiceGame
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
