// Code generated by go-swagger; DO NOT EDIT.

package users

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"

	"bitbucket.org/gank-global/clients/users/models"
)

// NewV2Params creates a new V2Params object
// with the default values initialized.
func NewV2Params() *V2Params {
	var ()
	return &V2Params{

		Timeout: cr.DefaultTimeout,
	}
}

// NewV2ParamsWithTimeout creates a new V2Params object
// with the default values initialized, and the ability to set a timeout on a request
func NewV2ParamsWithTimeout(timeout time.Duration) *V2Params {
	var ()
	return &V2Params{

		Timeout: timeout,
	}
}

// NewV2ParamsWithContext creates a new V2Params object
// with the default values initialized, and the ability to set a context for a request
func NewV2ParamsWithContext(ctx context.Context) *V2Params {
	var ()
	return &V2Params{

		Context: ctx,
	}
}

// NewV2ParamsWithHTTPClient creates a new V2Params object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewV2ParamsWithHTTPClient(client *http.Client) *V2Params {
	var ()
	return &V2Params{
		HTTPClient: client,
	}
}

/*V2Params contains all the parameters to send to the API endpoint
for the v2 operation typically these are written to a http.Request
*/
type V2Params struct {

	/*Content
	  User payload

	*/
	Content *models.UserPatchInput
	/*ID
	  User ID

	*/
	ID string

	Timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
	SMUserID   string
}

// WithTimeout adds the timeout to the v2 params
func (o *V2Params) WithTimeout(timeout time.Duration) *V2Params {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the v2 params
func (o *V2Params) SetTimeout(timeout time.Duration) {
	o.Timeout = timeout
}

// WithContext adds the context to the v2 params
func (o *V2Params) WithContext(ctx context.Context) *V2Params {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the v2 params
func (o *V2Params) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the v2 params
func (o *V2Params) WithHTTPClient(client *http.Client) *V2Params {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the v2 params
func (o *V2Params) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithContent adds the content to the v2 params
func (o *V2Params) WithContent(content *models.UserPatchInput) *V2Params {
	o.SetContent(content)
	return o
}

// SetContent adds the content to the v2 params
func (o *V2Params) SetContent(content *models.UserPatchInput) {
	o.Content = content
}

// WithID adds the id to the v2 params
func (o *V2Params) WithID(id string) *V2Params {
	o.SetID(id)
	return o
}

// SetID adds the id to the v2 params
func (o *V2Params) SetID(id string) {
	o.ID = id
}

// WriteToRequest writes these params to a swagger request
func (o *V2Params) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {
	// header param ID
	if o.SMUserID != "" {
		if err := r.SetHeaderParam("X-Sm-User-ID", o.SMUserID); err != nil {
			return err
		}
	}
	if err := r.SetTimeout(o.Timeout); err != nil {
		return err
	}
	var res []error

	if o.Content != nil {
		if err := r.SetBodyParam(o.Content); err != nil {
			return err
		}
	}

	// path param id
	if err := r.SetPathParam("id", o.ID); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
