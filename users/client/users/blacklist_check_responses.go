// Code generated by go-swagger; DO NOT EDIT.

package users

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"fmt"
	"io"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"

	"bitbucket.org/gank-global/clients/users/models"
)

// BlacklistCheckReader is a Reader for the BlacklistCheck structure.
type BlacklistCheckReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *BlacklistCheckReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 200:
		result := NewBlacklistCheckOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		result := NewResponseError()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result
	}
}

// NewBlacklistCheckOK creates a BlacklistCheckOK with default headers values
func NewBlacklistCheckOK() *BlacklistCheckOK {
	return &BlacklistCheckOK{}
}

/*BlacklistCheckOK handles this case with default header values.

OK
*/
type BlacklistCheckOK struct {
	Payload *BlacklistCheckOKBody
}

func (o *BlacklistCheckOK) Error() string {
	return fmt.Sprintf("[GET /users/blacklists/check][%d] blacklistCheckOK  %+v", 200, o.Payload)
}

func (o *BlacklistCheckOK) GetPayload() *BlacklistCheckOKBody {
	return o.Payload
}

func (o *BlacklistCheckOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(BlacklistCheckOKBody)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

/*BlacklistCheckOKBody blacklist check o k body
swagger:model BlacklistCheckOKBody
*/

type BlacklistCheckOKBody struct {

	// data
	Data *models.IsBlacklistResponse `json:"data,omitempty"`
}

// Validate validates this blacklist check o k body
func (o *BlacklistCheckOKBody) Validate(formats strfmt.Registry) error {
	var res []error

	if err := o.validateData(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *BlacklistCheckOKBody) validateData(formats strfmt.Registry) error {
	if swag.IsZero(o.Data) { // not required
		return nil
	}

	if o.Data != nil {
		if err := o.Data.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("blacklistCheckOK" + "." + "data")
			}
			return err
		}
	}

	return nil
}

// ContextValidate validate this blacklist check o k body based on the context it is used
func (o *BlacklistCheckOKBody) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	var res []error

	if err := o.contextValidateData(ctx, formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *BlacklistCheckOKBody) contextValidateData(ctx context.Context, formats strfmt.Registry) error {

	if o.Data != nil {
		if err := o.Data.ContextValidate(ctx, formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("blacklistCheckOK" + "." + "data")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (o *BlacklistCheckOKBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *BlacklistCheckOKBody) UnmarshalBinary(b []byte) error {
	var res BlacklistCheckOKBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}
