// Code generated by go-swagger; DO NOT EDIT.

package users

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetUserSummaryByIDParams creates a new GetUserSummaryByIDParams object
// with the default values initialized.
func NewGetUserSummaryByIDParams() *GetUserSummaryByIDParams {
	var ()
	return &GetUserSummaryByIDParams{

		Timeout: cr.DefaultTimeout,
	}
}

// NewGetUserSummaryByIDParamsWithTimeout creates a new GetUserSummaryByIDParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetUserSummaryByIDParamsWithTimeout(timeout time.Duration) *GetUserSummaryByIDParams {
	var ()
	return &GetUserSummaryByIDParams{

		Timeout: timeout,
	}
}

// NewGetUserSummaryByIDParamsWithContext creates a new GetUserSummaryByIDParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetUserSummaryByIDParamsWithContext(ctx context.Context) *GetUserSummaryByIDParams {
	var ()
	return &GetUserSummaryByIDParams{

		Context: ctx,
	}
}

// NewGetUserSummaryByIDParamsWithHTTPClient creates a new GetUserSummaryByIDParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetUserSummaryByIDParamsWithHTTPClient(client *http.Client) *GetUserSummaryByIDParams {
	var ()
	return &GetUserSummaryByIDParams{
		HTTPClient: client,
	}
}

/*GetUserSummaryByIDParams contains all the parameters to send to the API endpoint
for the get user summary by ID operation typically these are written to a http.Request
*/
type GetUserSummaryByIDParams struct {

	/*ID
	  User ID

	*/
	ID string

	Timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
	SMUserID   string
}

// WithTimeout adds the timeout to the get user summary by ID params
func (o *GetUserSummaryByIDParams) WithTimeout(timeout time.Duration) *GetUserSummaryByIDParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get user summary by ID params
func (o *GetUserSummaryByIDParams) SetTimeout(timeout time.Duration) {
	o.Timeout = timeout
}

// WithContext adds the context to the get user summary by ID params
func (o *GetUserSummaryByIDParams) WithContext(ctx context.Context) *GetUserSummaryByIDParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get user summary by ID params
func (o *GetUserSummaryByIDParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get user summary by ID params
func (o *GetUserSummaryByIDParams) WithHTTPClient(client *http.Client) *GetUserSummaryByIDParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get user summary by ID params
func (o *GetUserSummaryByIDParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithID adds the id to the get user summary by ID params
func (o *GetUserSummaryByIDParams) WithID(id string) *GetUserSummaryByIDParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the get user summary by ID params
func (o *GetUserSummaryByIDParams) SetID(id string) {
	o.ID = id
}

// WriteToRequest writes these params to a swagger request
func (o *GetUserSummaryByIDParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {
	// header param ID
	if o.SMUserID != "" {
		if err := r.SetHeaderParam("X-Sm-User-ID", o.SMUserID); err != nil {
			return err
		}
	}
	if err := r.SetTimeout(o.Timeout); err != nil {
		return err
	}
	var res []error

	// path param id
	if err := r.SetPathParam("id", o.ID); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
