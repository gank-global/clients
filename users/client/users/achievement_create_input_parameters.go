// Code generated by go-swagger; DO NOT EDIT.

package users

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"

	"bitbucket.org/gank-global/clients/users/models"
)

// NewAchievementCreateInputParams creates a new AchievementCreateInputParams object
// with the default values initialized.
func NewAchievementCreateInputParams() *AchievementCreateInputParams {
	var ()
	return &AchievementCreateInputParams{

		Timeout: cr.DefaultTimeout,
	}
}

// NewAchievementCreateInputParamsWithTimeout creates a new AchievementCreateInputParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewAchievementCreateInputParamsWithTimeout(timeout time.Duration) *AchievementCreateInputParams {
	var ()
	return &AchievementCreateInputParams{

		Timeout: timeout,
	}
}

// NewAchievementCreateInputParamsWithContext creates a new AchievementCreateInputParams object
// with the default values initialized, and the ability to set a context for a request
func NewAchievementCreateInputParamsWithContext(ctx context.Context) *AchievementCreateInputParams {
	var ()
	return &AchievementCreateInputParams{

		Context: ctx,
	}
}

// NewAchievementCreateInputParamsWithHTTPClient creates a new AchievementCreateInputParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewAchievementCreateInputParamsWithHTTPClient(client *http.Client) *AchievementCreateInputParams {
	var ()
	return &AchievementCreateInputParams{
		HTTPClient: client,
	}
}

/*AchievementCreateInputParams contains all the parameters to send to the API endpoint
for the achievement create input operation typically these are written to a http.Request
*/
type AchievementCreateInputParams struct {

	/*Content
	  Achievement payload

	*/
	Content *models.AchievementCreateInput
	/*ID
	  User ID

	*/
	ID string

	Timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
	SMUserID   string
}

// WithTimeout adds the timeout to the achievement create input params
func (o *AchievementCreateInputParams) WithTimeout(timeout time.Duration) *AchievementCreateInputParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the achievement create input params
func (o *AchievementCreateInputParams) SetTimeout(timeout time.Duration) {
	o.Timeout = timeout
}

// WithContext adds the context to the achievement create input params
func (o *AchievementCreateInputParams) WithContext(ctx context.Context) *AchievementCreateInputParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the achievement create input params
func (o *AchievementCreateInputParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the achievement create input params
func (o *AchievementCreateInputParams) WithHTTPClient(client *http.Client) *AchievementCreateInputParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the achievement create input params
func (o *AchievementCreateInputParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithContent adds the content to the achievement create input params
func (o *AchievementCreateInputParams) WithContent(content *models.AchievementCreateInput) *AchievementCreateInputParams {
	o.SetContent(content)
	return o
}

// SetContent adds the content to the achievement create input params
func (o *AchievementCreateInputParams) SetContent(content *models.AchievementCreateInput) {
	o.Content = content
}

// WithID adds the id to the achievement create input params
func (o *AchievementCreateInputParams) WithID(id string) *AchievementCreateInputParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the achievement create input params
func (o *AchievementCreateInputParams) SetID(id string) {
	o.ID = id
}

// WriteToRequest writes these params to a swagger request
func (o *AchievementCreateInputParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {
	// header param ID
	if o.SMUserID != "" {
		if err := r.SetHeaderParam("X-Sm-User-ID", o.SMUserID); err != nil {
			return err
		}
	}
	if err := r.SetTimeout(o.Timeout); err != nil {
		return err
	}
	var res []error

	if o.Content != nil {
		if err := r.SetBodyParam(o.Content); err != nil {
			return err
		}
	}

	// path param id
	if err := r.SetPathParam("id", o.ID); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
