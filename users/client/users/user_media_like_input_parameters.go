// Code generated by go-swagger; DO NOT EDIT.

package users

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewUserMediaLikeInputParams creates a new UserMediaLikeInputParams object
// with the default values initialized.
func NewUserMediaLikeInputParams() *UserMediaLikeInputParams {
	var ()
	return &UserMediaLikeInputParams{

		Timeout: cr.DefaultTimeout,
	}
}

// NewUserMediaLikeInputParamsWithTimeout creates a new UserMediaLikeInputParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewUserMediaLikeInputParamsWithTimeout(timeout time.Duration) *UserMediaLikeInputParams {
	var ()
	return &UserMediaLikeInputParams{

		Timeout: timeout,
	}
}

// NewUserMediaLikeInputParamsWithContext creates a new UserMediaLikeInputParams object
// with the default values initialized, and the ability to set a context for a request
func NewUserMediaLikeInputParamsWithContext(ctx context.Context) *UserMediaLikeInputParams {
	var ()
	return &UserMediaLikeInputParams{

		Context: ctx,
	}
}

// NewUserMediaLikeInputParamsWithHTTPClient creates a new UserMediaLikeInputParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewUserMediaLikeInputParamsWithHTTPClient(client *http.Client) *UserMediaLikeInputParams {
	var ()
	return &UserMediaLikeInputParams{
		HTTPClient: client,
	}
}

/*UserMediaLikeInputParams contains all the parameters to send to the API endpoint
for the user media like input operation typically these are written to a http.Request
*/
type UserMediaLikeInputParams struct {

	/*ID
	  Media ID

	*/
	ID string

	Timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
	SMUserID   string
}

// WithTimeout adds the timeout to the user media like input params
func (o *UserMediaLikeInputParams) WithTimeout(timeout time.Duration) *UserMediaLikeInputParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the user media like input params
func (o *UserMediaLikeInputParams) SetTimeout(timeout time.Duration) {
	o.Timeout = timeout
}

// WithContext adds the context to the user media like input params
func (o *UserMediaLikeInputParams) WithContext(ctx context.Context) *UserMediaLikeInputParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the user media like input params
func (o *UserMediaLikeInputParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the user media like input params
func (o *UserMediaLikeInputParams) WithHTTPClient(client *http.Client) *UserMediaLikeInputParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the user media like input params
func (o *UserMediaLikeInputParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithID adds the id to the user media like input params
func (o *UserMediaLikeInputParams) WithID(id string) *UserMediaLikeInputParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the user media like input params
func (o *UserMediaLikeInputParams) SetID(id string) {
	o.ID = id
}

// WriteToRequest writes these params to a swagger request
func (o *UserMediaLikeInputParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {
	// header param ID
	if o.SMUserID != "" {
		if err := r.SetHeaderParam("X-Sm-User-ID", o.SMUserID); err != nil {
			return err
		}
	}
	if err := r.SetTimeout(o.Timeout); err != nil {
		return err
	}
	var res []error

	// path param id
	if err := r.SetPathParam("id", o.ID); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
