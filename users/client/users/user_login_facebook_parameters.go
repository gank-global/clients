// Code generated by go-swagger; DO NOT EDIT.

package users

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"

	"bitbucket.org/gank-global/clients/users/models"
)

// NewUserLoginFacebookParams creates a new UserLoginFacebookParams object
// with the default values initialized.
func NewUserLoginFacebookParams() *UserLoginFacebookParams {
	var ()
	return &UserLoginFacebookParams{

		Timeout: cr.DefaultTimeout,
	}
}

// NewUserLoginFacebookParamsWithTimeout creates a new UserLoginFacebookParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewUserLoginFacebookParamsWithTimeout(timeout time.Duration) *UserLoginFacebookParams {
	var ()
	return &UserLoginFacebookParams{

		Timeout: timeout,
	}
}

// NewUserLoginFacebookParamsWithContext creates a new UserLoginFacebookParams object
// with the default values initialized, and the ability to set a context for a request
func NewUserLoginFacebookParamsWithContext(ctx context.Context) *UserLoginFacebookParams {
	var ()
	return &UserLoginFacebookParams{

		Context: ctx,
	}
}

// NewUserLoginFacebookParamsWithHTTPClient creates a new UserLoginFacebookParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewUserLoginFacebookParamsWithHTTPClient(client *http.Client) *UserLoginFacebookParams {
	var ()
	return &UserLoginFacebookParams{
		HTTPClient: client,
	}
}

/*UserLoginFacebookParams contains all the parameters to send to the API endpoint
for the user login facebook operation typically these are written to a http.Request
*/
type UserLoginFacebookParams struct {

	/*Content
	  User login payload

	*/
	Content *models.SocialNetworkLogin

	Timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
	SMUserID   string
}

// WithTimeout adds the timeout to the user login facebook params
func (o *UserLoginFacebookParams) WithTimeout(timeout time.Duration) *UserLoginFacebookParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the user login facebook params
func (o *UserLoginFacebookParams) SetTimeout(timeout time.Duration) {
	o.Timeout = timeout
}

// WithContext adds the context to the user login facebook params
func (o *UserLoginFacebookParams) WithContext(ctx context.Context) *UserLoginFacebookParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the user login facebook params
func (o *UserLoginFacebookParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the user login facebook params
func (o *UserLoginFacebookParams) WithHTTPClient(client *http.Client) *UserLoginFacebookParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the user login facebook params
func (o *UserLoginFacebookParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithContent adds the content to the user login facebook params
func (o *UserLoginFacebookParams) WithContent(content *models.SocialNetworkLogin) *UserLoginFacebookParams {
	o.SetContent(content)
	return o
}

// SetContent adds the content to the user login facebook params
func (o *UserLoginFacebookParams) SetContent(content *models.SocialNetworkLogin) {
	o.Content = content
}

// WriteToRequest writes these params to a swagger request
func (o *UserLoginFacebookParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {
	// header param ID
	if o.SMUserID != "" {
		if err := r.SetHeaderParam("X-Sm-User-ID", o.SMUserID); err != nil {
			return err
		}
	}
	if err := r.SetTimeout(o.Timeout); err != nil {
		return err
	}
	var res []error

	if o.Content != nil {
		if err := r.SetBodyParam(o.Content); err != nil {
			return err
		}
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
