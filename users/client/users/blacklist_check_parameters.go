// Code generated by go-swagger; DO NOT EDIT.

package users

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewBlacklistCheckParams creates a new BlacklistCheckParams object
// with the default values initialized.
func NewBlacklistCheckParams() *BlacklistCheckParams {
	var ()
	return &BlacklistCheckParams{

		Timeout: cr.DefaultTimeout,
	}
}

// NewBlacklistCheckParamsWithTimeout creates a new BlacklistCheckParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewBlacklistCheckParamsWithTimeout(timeout time.Duration) *BlacklistCheckParams {
	var ()
	return &BlacklistCheckParams{

		Timeout: timeout,
	}
}

// NewBlacklistCheckParamsWithContext creates a new BlacklistCheckParams object
// with the default values initialized, and the ability to set a context for a request
func NewBlacklistCheckParamsWithContext(ctx context.Context) *BlacklistCheckParams {
	var ()
	return &BlacklistCheckParams{

		Context: ctx,
	}
}

// NewBlacklistCheckParamsWithHTTPClient creates a new BlacklistCheckParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewBlacklistCheckParamsWithHTTPClient(client *http.Client) *BlacklistCheckParams {
	var ()
	return &BlacklistCheckParams{
		HTTPClient: client,
	}
}

/*BlacklistCheckParams contains all the parameters to send to the API endpoint
for the blacklist check operation typically these are written to a http.Request
*/
type BlacklistCheckParams struct {

	/*CheckingUserID
	  checking user id

	*/
	CheckingUserID string
	/*UserID
	  userid

	*/
	UserID string

	Timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
	SMUserID   string
}

// WithTimeout adds the timeout to the blacklist check params
func (o *BlacklistCheckParams) WithTimeout(timeout time.Duration) *BlacklistCheckParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the blacklist check params
func (o *BlacklistCheckParams) SetTimeout(timeout time.Duration) {
	o.Timeout = timeout
}

// WithContext adds the context to the blacklist check params
func (o *BlacklistCheckParams) WithContext(ctx context.Context) *BlacklistCheckParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the blacklist check params
func (o *BlacklistCheckParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the blacklist check params
func (o *BlacklistCheckParams) WithHTTPClient(client *http.Client) *BlacklistCheckParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the blacklist check params
func (o *BlacklistCheckParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithCheckingUserID adds the checkingUserID to the blacklist check params
func (o *BlacklistCheckParams) WithCheckingUserID(checkingUserID string) *BlacklistCheckParams {
	o.SetCheckingUserID(checkingUserID)
	return o
}

// SetCheckingUserID adds the checkingUserId to the blacklist check params
func (o *BlacklistCheckParams) SetCheckingUserID(checkingUserID string) {
	o.CheckingUserID = checkingUserID
}

// WithUserID adds the userID to the blacklist check params
func (o *BlacklistCheckParams) WithUserID(userID string) *BlacklistCheckParams {
	o.SetUserID(userID)
	return o
}

// SetUserID adds the userId to the blacklist check params
func (o *BlacklistCheckParams) SetUserID(userID string) {
	o.UserID = userID
}

// WriteToRequest writes these params to a swagger request
func (o *BlacklistCheckParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {
	// header param ID
	if o.SMUserID != "" {
		if err := r.SetHeaderParam("X-Sm-User-ID", o.SMUserID); err != nil {
			return err
		}
	}
	if err := r.SetTimeout(o.Timeout); err != nil {
		return err
	}
	var res []error

	// query param checkingUserID
	qrCheckingUserID := o.CheckingUserID
	qCheckingUserID := qrCheckingUserID
	if qCheckingUserID != "" {
		if err := r.SetQueryParam("checkingUserID", qCheckingUserID); err != nil {
			return err
		}
	}

	// query param userID
	qrUserID := o.UserID
	qUserID := qrUserID
	if qUserID != "" {
		if err := r.SetQueryParam("userID", qUserID); err != nil {
			return err
		}
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
