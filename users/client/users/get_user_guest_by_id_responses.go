// Code generated by go-swagger; DO NOT EDIT.

package users

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"fmt"
	"io"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"

	"bitbucket.org/gank-global/clients/users/models"
)

// GetUserGuestByIDReader is a Reader for the GetUserGuestByID structure.
type GetUserGuestByIDReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetUserGuestByIDReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 200:
		result := NewGetUserGuestByIDOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		result := NewResponseError()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result
	}
}

// NewGetUserGuestByIDOK creates a GetUserGuestByIDOK with default headers values
func NewGetUserGuestByIDOK() *GetUserGuestByIDOK {
	return &GetUserGuestByIDOK{}
}

/*GetUserGuestByIDOK handles this case with default header values.

OK
*/
type GetUserGuestByIDOK struct {
	Payload *GetUserGuestByIDOKBody
}

func (o *GetUserGuestByIDOK) Error() string {
	return fmt.Sprintf("[GET /users/guests/{id}][%d] getUserGuestByIdOK  %+v", 200, o.Payload)
}

func (o *GetUserGuestByIDOK) GetPayload() *GetUserGuestByIDOKBody {
	return o.Payload
}

func (o *GetUserGuestByIDOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(GetUserGuestByIDOKBody)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

/*GetUserGuestByIDOKBody get user guest by ID o k body
swagger:model GetUserGuestByIDOKBody
*/

type GetUserGuestByIDOKBody struct {

	// data
	Data *models.UserGuestResponse `json:"data,omitempty"`
}

// Validate validates this get user guest by ID o k body
func (o *GetUserGuestByIDOKBody) Validate(formats strfmt.Registry) error {
	var res []error

	if err := o.validateData(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *GetUserGuestByIDOKBody) validateData(formats strfmt.Registry) error {
	if swag.IsZero(o.Data) { // not required
		return nil
	}

	if o.Data != nil {
		if err := o.Data.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("getUserGuestByIdOK" + "." + "data")
			}
			return err
		}
	}

	return nil
}

// ContextValidate validate this get user guest by ID o k body based on the context it is used
func (o *GetUserGuestByIDOKBody) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	var res []error

	if err := o.contextValidateData(ctx, formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *GetUserGuestByIDOKBody) contextValidateData(ctx context.Context, formats strfmt.Registry) error {

	if o.Data != nil {
		if err := o.Data.ContextValidate(ctx, formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("getUserGuestByIdOK" + "." + "data")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (o *GetUserGuestByIDOKBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *GetUserGuestByIDOKBody) UnmarshalBinary(b []byte) error {
	var res GetUserGuestByIDOKBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}
