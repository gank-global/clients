// Code generated by go-swagger; DO NOT EDIT.

package users

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"fmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"

	"bitbucket.org/gank-global/clients/users/models"
)

// UserLogoutReader is a Reader for the UserLogout structure.
type UserLogoutReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *UserLogoutReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 200:
		result := NewUserLogoutOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		result := NewResponseError()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result
	}
}

// NewUserLogoutOK creates a UserLogoutOK with default headers values
func NewUserLogoutOK() *UserLogoutOK {
	return &UserLogoutOK{}
}

/*UserLogoutOK handles this case with default header values.

OK
*/
type UserLogoutOK struct {
}

func (o *UserLogoutOK) Error() string {
	return fmt.Sprintf("[POST /users/logout][%d] userLogoutOK ", 200)
}

func (o *UserLogoutOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}

/*UserLogoutBadRequestBody user logout bad request body
swagger:model UserLogoutBadRequestBody
*/

type UserLogoutBadRequestBody struct {

	// errors
	Errors *models.ErrorResponse `json:"errors,omitempty"`
}

// Validate validates this user logout bad request body
func (o *UserLogoutBadRequestBody) Validate(formats strfmt.Registry) error {
	var res []error

	if err := o.validateErrors(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *UserLogoutBadRequestBody) validateErrors(formats strfmt.Registry) error {
	if swag.IsZero(o.Errors) { // not required
		return nil
	}

	if o.Errors != nil {
		if err := o.Errors.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("userLogoutBadRequest" + "." + "errors")
			}
			return err
		}
	}

	return nil
}

// ContextValidate validate this user logout bad request body based on the context it is used
func (o *UserLogoutBadRequestBody) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	var res []error

	if err := o.contextValidateErrors(ctx, formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *UserLogoutBadRequestBody) contextValidateErrors(ctx context.Context, formats strfmt.Registry) error {

	if o.Errors != nil {
		if err := o.Errors.ContextValidate(ctx, formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("userLogoutBadRequest" + "." + "errors")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (o *UserLogoutBadRequestBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *UserLogoutBadRequestBody) UnmarshalBinary(b []byte) error {
	var res UserLogoutBadRequestBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}

/*UserLogoutInternalServerErrorBody user logout internal server error body
swagger:model UserLogoutInternalServerErrorBody
*/

type UserLogoutInternalServerErrorBody struct {

	// errors
	Errors *models.ErrorResponse `json:"errors,omitempty"`
}

// Validate validates this user logout internal server error body
func (o *UserLogoutInternalServerErrorBody) Validate(formats strfmt.Registry) error {
	var res []error

	if err := o.validateErrors(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *UserLogoutInternalServerErrorBody) validateErrors(formats strfmt.Registry) error {
	if swag.IsZero(o.Errors) { // not required
		return nil
	}

	if o.Errors != nil {
		if err := o.Errors.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("userLogoutInternalServerError" + "." + "errors")
			}
			return err
		}
	}

	return nil
}

// ContextValidate validate this user logout internal server error body based on the context it is used
func (o *UserLogoutInternalServerErrorBody) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	var res []error

	if err := o.contextValidateErrors(ctx, formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *UserLogoutInternalServerErrorBody) contextValidateErrors(ctx context.Context, formats strfmt.Registry) error {

	if o.Errors != nil {
		if err := o.Errors.ContextValidate(ctx, formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("userLogoutInternalServerError" + "." + "errors")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (o *UserLogoutInternalServerErrorBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *UserLogoutInternalServerErrorBody) UnmarshalBinary(b []byte) error {
	var res UserLogoutInternalServerErrorBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}

/*UserLogoutUnprocessableEntityBody user logout unprocessable entity body
swagger:model UserLogoutUnprocessableEntityBody
*/

type UserLogoutUnprocessableEntityBody struct {

	// errors
	Errors *models.ErrorResponse `json:"errors,omitempty"`
}

// Validate validates this user logout unprocessable entity body
func (o *UserLogoutUnprocessableEntityBody) Validate(formats strfmt.Registry) error {
	var res []error

	if err := o.validateErrors(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *UserLogoutUnprocessableEntityBody) validateErrors(formats strfmt.Registry) error {
	if swag.IsZero(o.Errors) { // not required
		return nil
	}

	if o.Errors != nil {
		if err := o.Errors.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("userLogoutUnprocessableEntity" + "." + "errors")
			}
			return err
		}
	}

	return nil
}

// ContextValidate validate this user logout unprocessable entity body based on the context it is used
func (o *UserLogoutUnprocessableEntityBody) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	var res []error

	if err := o.contextValidateErrors(ctx, formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *UserLogoutUnprocessableEntityBody) contextValidateErrors(ctx context.Context, formats strfmt.Registry) error {

	if o.Errors != nil {
		if err := o.Errors.ContextValidate(ctx, formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("userLogoutUnprocessableEntity" + "." + "errors")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (o *UserLogoutUnprocessableEntityBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *UserLogoutUnprocessableEntityBody) UnmarshalBinary(b []byte) error {
	var res UserLogoutUnprocessableEntityBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}
