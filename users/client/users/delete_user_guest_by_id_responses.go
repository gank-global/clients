// Code generated by go-swagger; DO NOT EDIT.

package users

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"
)

// DeleteUserGuestByIDReader is a Reader for the DeleteUserGuestByID structure.
type DeleteUserGuestByIDReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *DeleteUserGuestByIDReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 200:
		result := NewDeleteUserGuestByIDOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		result := NewResponseError()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result
	}
}

// NewDeleteUserGuestByIDOK creates a DeleteUserGuestByIDOK with default headers values
func NewDeleteUserGuestByIDOK() *DeleteUserGuestByIDOK {
	return &DeleteUserGuestByIDOK{}
}

/*DeleteUserGuestByIDOK handles this case with default header values.

OK
*/
type DeleteUserGuestByIDOK struct {
}

func (o *DeleteUserGuestByIDOK) Error() string {
	return fmt.Sprintf("[DELETE /users/guests/{id}][%d] deleteUserGuestByIdOK ", 200)
}

func (o *DeleteUserGuestByIDOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}
