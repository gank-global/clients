// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"

	"github.com/go-openapi/strfmt"
)

// MapClaims Claims type that uses the map[string]interface{} for JSON decoding
// This is the default claims type if you don't supply one
//
// swagger:model MapClaims
type MapClaims map[string]interface{}

// Validate validates this map claims
func (m MapClaims) Validate(formats strfmt.Registry) error {
	return nil
}

// ContextValidate validates this map claims based on context it is used
func (m MapClaims) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	return nil
}
