// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"

	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// UserSummaryCatalog user summary catalog
//
// swagger:model UserSummaryCatalog
type UserSummaryCatalog struct {

	// ID
	ID string `json:"ID,omitempty"`

	// name
	Name string `json:"Name,omitempty"`
}

// Validate validates this user summary catalog
func (m *UserSummaryCatalog) Validate(formats strfmt.Registry) error {
	return nil
}

// ContextValidate validates this user summary catalog based on context it is used
func (m *UserSummaryCatalog) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *UserSummaryCatalog) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *UserSummaryCatalog) UnmarshalBinary(b []byte) error {
	var res UserSummaryCatalog
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
