// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
	"github.com/go-openapi/validate"
)

// UserCreateInput user create input
//
// swagger:model UserCreateInput
type UserCreateInput struct {

	// active
	// Required: true
	Active *bool `json:"active"`

	// avatar
	Avatar string `json:"avatar,omitempty"`

	// birth date
	BirthDate string `json:"birthDate,omitempty"`

	// description
	Description string `json:"description,omitempty"`

	// device ID
	DeviceID string `json:"deviceID,omitempty"`

	// email
	Email string `json:"email,omitempty"`

	// first name
	FirstName string `json:"firstName,omitempty"`

	// gender
	Gender string `json:"gender,omitempty"`

	// in team
	InTeam bool `json:"inTeam,omitempty"`

	// last name
	LastName string `json:"lastName,omitempty"`

	// nickname
	// Required: true
	Nickname *string `json:"nickname"`

	// password
	// Required: true
	Password *string `json:"password"`

	// paypal
	Paypal *Paypal `json:"paypal,omitempty"`

	// profile
	Profile *UserProfile `json:"profile,omitempty"`

	// roles
	// Required: true
	Roles []string `json:"roles"`

	// team name
	TeamName string `json:"teamName,omitempty"`
}

// Validate validates this user create input
func (m *UserCreateInput) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateActive(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateNickname(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validatePassword(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validatePaypal(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateProfile(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateRoles(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *UserCreateInput) validateActive(formats strfmt.Registry) error {

	if err := validate.Required("active", "body", m.Active); err != nil {
		return err
	}

	return nil
}

func (m *UserCreateInput) validateNickname(formats strfmt.Registry) error {

	if err := validate.Required("nickname", "body", m.Nickname); err != nil {
		return err
	}

	return nil
}

func (m *UserCreateInput) validatePassword(formats strfmt.Registry) error {

	if err := validate.Required("password", "body", m.Password); err != nil {
		return err
	}

	return nil
}

func (m *UserCreateInput) validatePaypal(formats strfmt.Registry) error {
	if swag.IsZero(m.Paypal) { // not required
		return nil
	}

	if m.Paypal != nil {
		if err := m.Paypal.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("paypal")
			}
			return err
		}
	}

	return nil
}

func (m *UserCreateInput) validateProfile(formats strfmt.Registry) error {
	if swag.IsZero(m.Profile) { // not required
		return nil
	}

	if m.Profile != nil {
		if err := m.Profile.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("profile")
			}
			return err
		}
	}

	return nil
}

func (m *UserCreateInput) validateRoles(formats strfmt.Registry) error {

	if err := validate.Required("roles", "body", m.Roles); err != nil {
		return err
	}

	return nil
}

// ContextValidate validate this user create input based on the context it is used
func (m *UserCreateInput) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	var res []error

	if err := m.contextValidatePaypal(ctx, formats); err != nil {
		res = append(res, err)
	}

	if err := m.contextValidateProfile(ctx, formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *UserCreateInput) contextValidatePaypal(ctx context.Context, formats strfmt.Registry) error {

	if m.Paypal != nil {
		if err := m.Paypal.ContextValidate(ctx, formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("paypal")
			}
			return err
		}
	}

	return nil
}

func (m *UserCreateInput) contextValidateProfile(ctx context.Context, formats strfmt.Registry) error {

	if m.Profile != nil {
		if err := m.Profile.ContextValidate(ctx, formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("profile")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (m *UserCreateInput) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *UserCreateInput) UnmarshalBinary(b []byte) error {
	var res UserCreateInput
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
