// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
	"github.com/go-openapi/validate"
)

// UserGame user game
//
// swagger:model UserGame
type UserGame struct {

	// created at
	// Format: date-time
	CreatedAt strfmt.DateTime `json:"createdAt,omitempty"`

	// game ID
	GameID string `json:"gameID,omitempty"`

	// id
	ID string `json:"id,omitempty"`

	// skill level
	SkillLevel string `json:"skillLevel,omitempty"`

	// summary
	Summary string `json:"summary,omitempty"`

	// updated at
	// Format: date-time
	UpdatedAt strfmt.DateTime `json:"updatedAt,omitempty"`

	// user ID
	UserID string `json:"userID,omitempty"`

	// years played
	// Format: date-time
	YearsPlayed strfmt.DateTime `json:"yearsPlayed,omitempty"`
}

// Validate validates this user game
func (m *UserGame) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateCreatedAt(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateUpdatedAt(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateYearsPlayed(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *UserGame) validateCreatedAt(formats strfmt.Registry) error {
	if swag.IsZero(m.CreatedAt) { // not required
		return nil
	}

	if err := validate.FormatOf("createdAt", "body", "date-time", m.CreatedAt.String(), formats); err != nil {
		return err
	}

	return nil
}

func (m *UserGame) validateUpdatedAt(formats strfmt.Registry) error {
	if swag.IsZero(m.UpdatedAt) { // not required
		return nil
	}

	if err := validate.FormatOf("updatedAt", "body", "date-time", m.UpdatedAt.String(), formats); err != nil {
		return err
	}

	return nil
}

func (m *UserGame) validateYearsPlayed(formats strfmt.Registry) error {
	if swag.IsZero(m.YearsPlayed) { // not required
		return nil
	}

	if err := validate.FormatOf("yearsPlayed", "body", "date-time", m.YearsPlayed.String(), formats); err != nil {
		return err
	}

	return nil
}

// ContextValidate validates this user game based on context it is used
func (m *UserGame) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *UserGame) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *UserGame) UnmarshalBinary(b []byte) error {
	var res UserGame
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
